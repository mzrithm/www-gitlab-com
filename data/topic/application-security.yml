title: Application Security
header_body: >-
  Can your existing application security keep pace with modern development
  methods? Learn how next-generation software requires a new approach to app
  sec.


  [Download the ebook →](https://about.gitlab.com/resources/ebook-ciso-secure-software/)
description: Effectively turn your DevOps methodology into a DevSecOps methodology
canonical_path: /topics/application-security/
file_name: application-security
twitter_image: /images/opengraph/gitlab-blog-cover.png
related_content:
  - title: What is fuzz testing?
    url: /topics/application-security/what-is-fuzz-testing/
  - title: What is developer-first security?
    url: /topics/application-security/what-is-developer-first-security/
  - title: A DevSecOps security checklist
    url: /topics/application-security/devsecops-security-checklist/
  - title: A beginner's guide to container security
    url: https://about.gitlab.com/topics/application-security/beginners-guide-to-container-security/
  - url: https://about.gitlab.com/topics/application-security/three-steps-to-better-devsecops.html
    title: Three steps to better DevSecOps
cover_image: /images/topics/application-security.svg
body: >-
  ## What is application security?


  Application security has been [defined by TechTarget](https://searchsoftwarequality.techtarget.com/definition/application-security) as the use of software, hardware, and procedural methods to protect applications from external threats. Modern approaches include shifting left to find and fix vulnerabilities earlier, while also shifting right to protect your applications and their infrastructure-as-code in production. Securing the Software Development Life Cycle (SDLC) itself is often a requirement as well.


  This approach of building security into your development and operational processes effectively turns your DevOps methodology into a DevSecOps methodology. An end-to-end DevOps platform can best enable this approach.


  ## Why adopt DevSecOps?


  Security has traditionally come at the end of the development lifecycle, adding cost and time when code is inevitably sent back to the developer for fixes. It is time to shift left by embracing security earlier within the DevOps lifecycle.


  ![Cost savings of DevSecOps](/images/devsecops/cost-savings-to-shift-left.png)


  DevSecOps tools automate security workflows to create an adaptable process for your development and security teams, improving collaboration and breaking down silos. By embedding security into the SDLC, you can consistently secure fast- moving and iterative processes - improving efficiency without sacrificing quality.


  DevSecOps weaves security practices into every stage of software development right through deployment with the use of tools and methods to protect and monitor live applications. New attack surfaces such as containers and orchestrators must be monitored and protected alongside the application itself.


  ## DevSecOps fundamentals


  If you've read the book that was the genesis for the DevOps movement, The Phoenix Project, you understand the importance of automation, consistency, metrics, and collaboration. For DevSecOps, you are essentially applying these techniques to outfit the software factory while embedding security capabilities along the way rather than in a separate, siloed process. Dev or security can find vulnerabilities, but a developer is usually required to remove such flaws. It makes sense to empower them to find and fix vulnerabilities while they are still working on the code. Scanning alone isn't enough. It's about getting the results to the right people, at the right time, with the right context for quick action.


  Fundamental requirements include automation and collaboration, along with policy guardrails and visibility.


  ## Automation


  In our [2020 DevSecOps Survey](https://about.gitlab.com/developer-survey/) we found a majority of developers aren't running SAST, DAST or other security scans regularly, and automation also lags. A majority of security pros reported their DevOps teams are "shifting left," but test automation continues to be a huge challenge.


  ![]()


  ### Collaboration


  A single source of truth that reports vulnerabilities and remediation provides much-needed transparency to both development and security team. It can streamline cycles, eliminate friction, and remove unnecessary translation across tools.


  ### Policy guardrails


  Every enterprise has a different appetite for risk. Your security policies will reflect what is right for you while the regulatory requirements to which you must adhere will also influence the policies you must apply. Hand-in-hand with automation, guardrails can ensure consistent application of your security and compliance policies.


  ### Visibility


  An end-to-end DevSecOps platform can give auditors a clear view into who changed what, where, when, and why from beginning to end of the software lifecyle. Leveraging a single-source-of-truth can also ensure earlier visibility into application risks.


  ## Getting started


  Ready to see how GitLab can help you get started with DevSecOps?


  Our [DevSecOps Solution](https://about.gitlab.com/solutions/dev-sec-ops/) page has all of the details, along with a Free Trial offer for our Ultimate tier of capabilities.


  ## Assess your DevSecOps maturity


  We have developed a simple assessment that uses twenty questions to help you determine where you excel and areas for improvement. A helpful guide suggests steps to get you started.


  [Assess yourself  →](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/blob/master/design/publications/e-books/security-maturity-model/pdf/security-maturity-model.pdf)
benefits_title: Benefits of DevSecOps
benefits:
  - image: /images/icons/time-icon2.svg
    title: Speed
    description: Developers can remediate vulnerabilities while they’re coding,
      which teaches secure code writing and reduces back and forth during
      security reviews.
  - title: Collaboration
    description: Encouraging a security mindset across your app dev team aligns
      goals with security and encourages employees to work with others outside
      their functional silo.
    image: /images/icons/first-look-influence.svg
  - title: Efficiency
    description: DevSecOps saves time, money, and employee resources over every
      launch and iteration - all valuable assets to IT and security teams
      suffering from <a
      href="https://go.forrester.com/blogs/security-risk-2019-cybersecuritys-staffing-shortage-is-self-inflicted/">skills</a>
      and budget shortages.
    image: /images/icons/trending-icon2.svg
cta_banner:
  - title: Manage your toolchain before it manages you
    body: Visible, secure, and effective toolchains are difficult to come by due to
      the increasing number of tools teams use, and it’s placing strain on
      everyone involved. This study dives into the challenges, potential
      solutions, and key recommendations to manage this evolving complexity.
    cta:
      - text: Read the study
        url: https://about.gitlab.com/resources/whitepaper-forrester-manage-your-toolchain/
resources_title: Resources
resources_intro: >-
  Here is a list of resources on DevSecOps that we find to be particularly
  helpful. We would love to get your recommendations on books, blogs, videos,
  podcasts and other resources that offer valuable insight into best practices.


  Please share your favorites with us by tweeting us [@GitLab!](https://twitter.com/gitlab)
resources:
  - url: https://www.youtube.com/watch?v=4ROYvNfRZVU&feature=emb_logo
    title: Finding Bugs with Coverage Guided Fuzz Testing (DevSecOps)
    type: Video
  - url: https://page.gitlab.com/resources-article-RightPlatformDoD.html
    title: Best Practices for a DoD DevSecOps Culture
    type: Articles
  - title: Citizen-Centric Resiliency In Challenging Times
    url: https://page.gitlab.com/webcast-citizen-centric-resiliency.html
    type: Webcast
suggested_content:
  - url: /blog/2019/08/12/developer-intro-sast-dast/
  - url: /blog/2019/09/03/developers-write-secure-code-gitlab/
  - url: /blog/2019/09/16/security-testing-principles-developer/
schema_faq:
  - question: What is application security?
    answer: Application security has been defined by TechTarget as the use of
      software, hardware, and procedural methods to protect applications from
      external threats. Modern approaches include shifting left to find and fix
      vulnerabilities earlier, while also shifting right to protect your
      applications and their infrastructure-as-code in production. Securing the
      Software Development Life Cycle (SDLC) itself is often a requirement as
      well.
    cta:
      - text: Learn more about application security
        url: https://about.gitlab.com/topics/application-security/
