| Priority | Description | Issue label(s) |
| ------ | ------ | ------ |
| 1* | <a href="/handbook/engineering/security/#severity-and-priority-labels-on-security-issues">Security fixes</a> | `security` |
| 2 | Data-loss prevention | `data loss` |
| 3* | <a href="/handbook/engineering/performance/index.html#availability">Availability</a>, <a href="/handbook/engineering/workflow/#infradev">Infradev</a>, Incident Corrective Actions, [Sharding Blockers](https://about.gitlab.com/company/team/structure/working-groups/database-scalability/) | `availability`, `infradev`, `Corrective Action`, `sharding-blocker`  |
| 4 | Fixing regressions (things that worked before) | `regression` |
| 5 | Performance Refinement | `performance-refinement` |
| 6 | Promised to Customers | `planning-priority`, `customer`, `customer+` |
| 7 | Instrumentation improvements, particularly for xMAU | `instrumentation` |
| 8 | Usability Improvements and User Experience to drive xMAU |`feature::enhancement`, `UX debt`|
| 9 | IACV Drivers | |
| 10 | Identified for Dogfooding | `Dogfooding::Build in GitLab`, `Dogfooding::Rebuild in GitLab` |
| 11 | Velocity of new features, technical debt, community contributions, and all other improvements | `direction`, `feature`, `technical debt` |
| 12 | Behaviors that yield higher predictability (because this inevitably slows us down) | `predictability` |

*indicates forced prioritization items with SLAs/SLOs

### Engineering Allocation

Engineering is the DRI for mid/long term team efficiency, performance, security (incident response and anti-abuse capabilities), availability, and scalability. The expertise to proactively identify and iterate on these is squarely in the Engineering team. Whereas Product can support in performance issues as identified from customers. In some ways these efforts can be viewed as risk-mitigation or revenue protection. They also have the characteristic of being larger than one group at the stage level. Development would like to conduct an experiment to focus on initiatives that should help the organization scale appropriately in the long term.  We are treating these as a percent investment of time associated with a stage or category. The percent of investment time can be viewed as a prioritization budget outside normal Product/Development assignments.

Engineering Allocation is also used in short-term situations in conjunction and in support of maintaining acceptable Error Budgets for GitLab.com and our [GitLab-hosted first](/direction/#gitlab-hosted-first) theme.

Unless it is listed in this table, Engineering Allocation for a stage/group is 0% and we are following normal [prioritization](/handbook/product/product-processes/#prioritization). Refer to this [page](https://about.gitlab.com/handbook/engineering/engineering-allocation/) for Engineering Allocation charting efforts. Some stage/groups may be allocated at a high percentage or 100%, typically indicating a situation where all available effort is to be focused on Reliability related (top 5 priorities from [prioritization table](/handbook/product/product-processes/#prioritization)) work.

Mid/long term initiatives are engineering-led. The EM is responsible for recognizing the problem, creating a satisfactory goal with clear success criteria, developing a plan, executing on a plan and reporting status.  It is recommended that the EM collaborate with PMs in all phases of this effort as we want PMs to feel ownership for these challenges.  This could include considering adding more/less allocation, setting the goals to be more aspirational, reviewing metrics/results, etc.   We welcome strong partnerships in this area because we are one team even when allocations are needed for long-range activities.

During periods of Engineering Allocation, the PM remains the interface between the group and the fields teams & customers. This is important because:
- It allows Engineering to remain focused on the work at hand
- It maintains continuity for the field teams - they should not have to figure out different patterns of communication for the customer
- It keeps PMs fully informed about the product's readiness

| Group/Stage | Description of Goal | Justification | Maximum % of headcount budget | People | Supporting information | EMs / DRI | PMs |
| ------ | ------ | ------- | ------ | ------ | ------- |  ------ | ------ | 
| Manage:Access (BE)  | Infradev burn-down | Improve .com reliability and resolve infradev issues |  17%  | 1 | [List of issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&state=opened&label_name[]=group%3A%3Aaccess&label_name[]=infradev) | @lmcandrew | @ogolowinski  |
| Manage:Access (BE)  | Security burn-down | More security issues exist in the backlog and are incoming than the team is able to prioritize |  50%  | 3 | [List of issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&state=opened&label_name%5B%5D=security&label_name%5B%5D=group%3A%3Aaccess&not%5Blabel_name%5D%5B%5D=feature) | @dennis | @ogolowinski  |
| Manage:Access (BE)  | Scalability of AuthN/AuthZ functionality (Workspace) | Reduce duplication of code and increase performance for Groups/Projects |  17% | 1 | [Consolidate Groups and Projects](https://gitlab.com/groups/gitlab-org/-/epics/6473) | @m_gill | @ogolowinski  |
| Manage:Access (BE)  | Linear Namespace Queries | Replace recursive CTE queries, which are complex and unpredictable |  17% | 1 | [Linear Namespace Queries](https://gitlab.com/groups/gitlab-org/-/epics/5296) | @mksionek | @ogolowinski  |
| Manage:Workspace (BE) | floor % | empower every SWEs from raising reliability and security issues | 10% | 4 | N/A | @mksionek | @ogolowinski |
| Manage:Compliance (BE) | floor % | empower every SWEs from raising reliability and security issues | 10% | 1 | N/A | @djensen | @stkerr |
| Manage:Import (BE)  | Improving Error Budget, Infra-Dev Issues, and Security |  Backlog of security issues and production incidents tied to infradev | 50% | 2 | [Infradev & Security Issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&state=opened&label_name[]=group%3A%3Aimport&label_name[]=Engineering%20Allocation) | @lmcandrew |  @hdelalic  |
| Manage:Optimize (BE) |3 month headcount reset to help Manage:Access | 3 month headcount reset to help Manage:Access |  67% | 3 | 3 month headcount reset to help Manage:Access| @djensen | @ljlane | 
| Plan:Project management | 3 month headcount reset to help Manage:Access | 3 month headcount reset to help Manage:Access | 50% | 4 | 3 month headcount reset to help Manage:Access | @jlear | @gweaver |
| Plan:Product Planning | 3 month headcount reset to help Manage:Access | 3 month headcount reset to help Manage:Access | 40% | 5 | 3 month headcount reset to help Manage:Access | @johnhope | @cdybenko |
| Create:Source Code (BE) | Improving Error Budget and working Infra-Dev Issues | Improving Reliability of Source Code Features (infradev, improve error budgets, P1/S1 issues, security issues)| 100% | 4 | [Epic](https://gitlab.com/groups/gitlab-org/-/epics/6523) | @sean_carroll | @sarahwaldner    |
| Create:Code Review (BE) | Improving Error Budget and working Infra-Dev Issues | Improving Reliability of Code Review Features (infradev, improve error budgets, P1/S1 issues, security issues) | 100% | 5 | [Epic](https://gitlab.com/groups/gitlab-org/-/epics/6524) | @mnohr | @phikai   |
| Create:Editor (BE) | 3 month headcount reset to help Manage:Access | 3 month headcount reset to help Manage:Access | 100% | 2 | N/A | @oregand | @ericschurter |
| Create:Gitaly | Infra-Dev Issues, P1/S1 issues, security issues and Customer Escalations (engineering approved) | Improve reliability of Gitaly |  20%  | 6 |  [Infradev Issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&state=opened&label_name[]=infradev&label_name[]=group%3A%3Agitaly) | @timzallmann | @mjwood |
| Ecosystems:Integrations | 3 month headcount reset to help Manage:Access | 3 month headcount reset to help Manage:Access | 100% | 4 | 3 month headcount reset to help Manage:Access | @arturoherrero | @mushakov |
| Verify:Pipeline Execution | Improving Error Budget and working Infra-Dev Issues | Large backlog of Infra-Dev issues | 100% | 5 | [CI Category Direction](https://about.gitlab.com/direction/verify/continuous_integration/#whats-next--why) | @cheryl.li |  @jreporter  |
| Verify: Pipeline Authoring | floor % | empower every SWEs from raising reliability and security issues | 10% | 3 | N/A | Mark Nuzzo | @dhershkovitch |
| Verify:Runner | Improve Runner deployment to GitLab.com Shared Runners. Target reduction in time to deploy of 80%. | Increase operational responsiveness and team efficiency/standardization |  30% | 6 | [CD for Runner](https://gitlab.com/groups/gitlab-org/-/epics/1453) | @erushton | @DarrenEastman |
| Verify:Testing | Improving Error Budget and working Infra-Dev Issues | Allocation to Verify:Pipeline Execution Infradev Issues | 50% | 2 | [Testing Investigation Issue](https://gitlab.com/gitlab-org/gitlab/-/issues/331163)  |   @shampton  |   @jheimbuck_gl|
| Package:Package | floor % | empower every SWEs from raising reliability and security issues | 10% | 5 | N/A | @dcroft | @trizzi |
| Release:Release | 3 milestones manage:Import Headcount Reset | Unlocks a new CEO initiative | 17% | 1 | https://gitlab.com/gitlab-com/Product/-/issues/3062 | @nicolewilliams | @kbychu |
| Configure:Configure | 3 milestones manage:Import Headcount Reset | Unlocks a new CEO initiative  | 20% | 1 | https://gitlab.com/gitlab-com/Product/-/issues/3062 | @nicholasklick | @nagyv-gitlab |
| Configure:Configure | Headcount reset to help Ops:Pipeline Execution for milestones 14.3 and 14.4 | Assist with Infradev Issues | 20% | 1 | [Headcount Reset: Infradev Burndown in Verify and Reliability](https://gitlab.com/gitlab-com/Product/-/issues/2934) | @nicholasklick | @nagyv-gitlab |
| Monitor:Monitor | Headcount reset to help Ops:Pipeline Execution for milestones 14.3 and 14.4 | Assist with Infradev Issues | 40% | 2  | [Headcount Reset: Infradev Burndown in Verify and Reliability](https://gitlab.com/gitlab-com/Product/-/issues/2934) | @crystalpoole | @abellucci |
| Verify | Scale GitLab.com to 20M builds a day | Give us 2 years of runway | 10% | 1 | [CI Scaling Target](https://gitlab.com/groups/gitlab-org/-/epics/5745) | @grzesiek  | @jreporter |
| Secure:Static Analysis | floor % | empower every SWEs from raising reliability and security issues | 10% | 5 | N/A | @twoodham | @tmccaslin |
| Secure:Dynamic Analysis | floor % | empower every SWEs from raising reliability and security issues | 10% | 5 | N/A | @sethgitlab | @derekferguson |
| Secure:Composition Analysis | Proposed 3 month headcount reset to help manage:Import | Proposed 3 month headcount reset to help manage:Import | 25% | 4 | Proposed 3 month headcount reset to help manage:Import | @gonzoyumo  | @NicoleSchwartz |
| Secure:Threat Insights | floor % | empower every SWEs from raising reliability and security issues | 10% | 4 | N/A | @thiagocsf | @matt_wilson |
| Protect:Container Security | 3 months headcount reset to new staging environment | 3 months headcount reset to new staging environment | 25% | 4 | 3 months headcount reset to new staging environment | @thiagocsf | @sam.white |
| Growth:Activation | floor % | empower every SWEs from raising reliability and security issues | 10% | 1 | N/A | @pcalder | @jstava |
| Growth:Conversion | floor % | empower every SWEs from raising reliability and security issues | 10% | 2 | N/A | @pcalder | @s_awezec |
| Growth:Expansion | 3 month headcount reset to help manage: Import | 3 month headcount reset to help manage: Import | 50% | 2 | 3 month headcount reset to help manage: Import | @pcalder | @gdoud |
| Growth:Adoption | floor % | empower every SWEs from raising reliability and security issues | 10% | 2 | N/A | @pcalder | @mkarampalas |
| Growth:Product Intelligence | floor % | empower every SWEs from raising reliability and security issues | 10% | 6 | N/A | @nicolasdular | @amandarueda |
| Fulfillment | Improve availability of CustomersDot by migrating from Azure to GCP | Improve availability of CustomersDot due to [several recent outages](https://gitlab.com/gitlab-com/gl-infra/production/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=Service%3A%3ACustomers) | 20% (4 Fulfillment Engineers + 0.67 Infrastructure Engineers) | 20 | [Epic](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/390) | @jeromezng | @justinfarris |
| Fulfillment | Help Trust and Safety mitigate crypto-abuse by storing non-identifying credit card meta data | Provide Trust and Safety team with the ability to identify which abuse accounts were opened using the same credit card | 5% | 20 | [Issue](https://gitlab.com/gitlab-org/gitlab/-/issues/333177) | @jeromezng | @justinfarris |
| Enablement:Distribution | floor % | empower every SWEs from raising reliability and security issues | 10% | 9 | N/A | @mendeni | @dorrino |
| Enablement: Geo | 3 months headcount reset to new staging environment | 3 months headcount reset to new staging environment | 13% | 7 | 3 months headcount reset to new staging environment | @nhxnguyen | @nhxnguyen |
| Enablement:Database | Primary Key overflow, Retention Strategy, Schema Validation, migration improvements, testing | Database has been under heavy operational load and needs improvement | 100% | 5 | [Automated Migration testing](https://gitlab.com/groups/gitlab-org/database-team/-/epics/6), [Automated migrations for primary key conversions](https://gitlab.com/groups/gitlab-org/-/epics/5654), [Remove PK overflow](https://gitlab.com/groups/gitlab-org/-/epics/4785), [Schema Validation](https://gitlab.com/groups/gitlab-org/-/epics/3928), [Reduce Total size of DB](https://gitlab.com/groups/gitlab-org/-/epics/4181) | @craig-gomes | TBD |
| Enablement:Sharding | floor % | empower every SWEs from raising reliability and security issues | 10% | 4 | N/A | @craig-gomes | @fzimmer |
| Enablement:Memory | Scalability-2 Swarm on Redis | Redis is one of our top scaling bottlenecks | 25% | 1 | [Move Rack::Attack store to its own Redis instance](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/526) | @changzhengliu | @iroussos |
| Enablement:Memory | Improve Redis Scalability | Redis is one of our top scaling bottlenecks | 75% | 3 | [Functionally partition Redis](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/12821) | @changzhengliu | @iroussos |
| Enablement:Global Search | Enhance security in GitLab application | Security is the top priority in [Prioritizing technical decisions](https://about.gitlab.com/handbook/engineering/#prioritizing-technical-decisions) | 10% | 1 | [Security related issue](https://gitlab.com/gitlab-org/gitlab/-/issues/339169) | @changzhengliu | @JohnMcGuire |

#### Broadcasting and communication of Engineering Allocation direction

Each allocation has a [direction page](/handbook/product/product-processes/#managing-your-product-direction) maintained by the Engineering Manager. The Engineering Manager will provide regular updates to the direction page. Steps to add a direction page are:

1. Open an MR to the [direction content](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/source/direction/)
1. Add a directory under the correct stage named for the title Engineering Allocation
1. Add a file for the page named `index.html.md` in the newly created directory

To see an example for an Engineering Allocation Direction page, see [Continuous Integration Scaling](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/source/direction/verify/continuous_integration_scaling/index.html.md). Once the Engineering Allocation is complete, delete the direction page.

#### How to get a effort added to Engineering Allocation

One of the most frequent questions we get as part of this experiment is "How does a problem get put on the Engineering Allocation list?".  The short answer is someone makes a suggestion and we add it.  Much like everyone can contribute, we would like the feedback loop for improvement and long terms goals to be robust.  So everyone should feel the empowerment to suggest an item at any time.

To help with getting items that on the list for consideration, we will be performing a survey periodically.  The survey will consist of the following questions:

1. If you were given a % of engineering development per release to work on something, what would it be?
1. How would you justify it?

We will keep the list of questions short to solicit the most input.  The survey will go out to members of the Development, Quality, Security.  After we get the results, we will consider items for potential adding as an Engineering Allocation.


#### Closing out Engineering Allocation items

Once the item's success criteria are achieved, the Engineering Manager should consult with counterparts to review whether the improvements are sustainable. Where appropriate, we should consider adding monitoring and alerting to any areas of concern that will allow us to make proactive prioritizations in future should the need arise. The Engineering Manager should close all related epics/issues, remove the allocation from the above table, and inform the Product Manager when the allocated capacity will be available to return their focus to product prioritizations.

All engineering allocation removals should be reviewed and approved by the [VP of Development](https://about.gitlab.com/handbook/engineering/development/#team-members).

### Feature Change Locks

We will enact a localized feature change lock (FCL) anytime there is an S1 or public-facing (tweeted) S2 incident on GitLab.com (including the License App, CustomersDot, and Versions) determined to be caused by a change from the development department. The [team](https://about.gitlab.com/company/team/structure/#organizational-structure) involved should be determined by the author, their line manager, and that manager's other direct reports. Attribution would fall to the VP of Infrastructure (if not obvious). The intent is to create a sense of ownership and accountability amongst our teams, but this should not challenge our no-blame culture.

The FCL will last 5 business days. During this time, the team(s) exclusive focus is around reliability work, and any other type of work in-flight has to be paused.  The team(s) must:

* Create a public slack channel called `#fcl-incident-[number]`, with members
    * The Team's Manager
    * The Author and their teammates
    * All reviewer(s)
    * All maintainers(s)
    * Infrastructure Stable counterpart
    * The chain-of-command from the manager to the CTO (Sr Manager, Sr/Director, VP of Development, CTO, etc)
* Fully complete a Root Cause Analysis (RCA) on day one.  The RCA must include completing all fields in the related incident issue (see [incident issue template](https://gitlab.com/gitlab-com/gl-infra/production/-/blob/master/.gitlab/issue_templates/incident.md)).  The incident issue should serve as the single source of truth for the RCA. Completing it should create a common understanding of the problem space and set a shared direction for the work that needs to be completed.
* A work plan referencing all the Issues, Epics, and/or involved MRs must be created and used to identify the scope of work for the FCL.

After the RCA is completed, the team(s) focus is on:

* Address immediate corrective actions to prevent incident reoccurrence in the short term
* Introduce changes to reduce incident detection time (Improve collected metrics, and the service level monitoring)
* Introduce changes to reduce mitigation time (Improve rollout process through feature flags, and clean rollbacks)
* Ensure that the incident is reproducible in environments outside of production (Detect issues in staging, increase end-to-end integration test coverage)
* Improve development test coverage (Harden unit testing, make it simpler to detect problems during reviews) 

Examples of this work include, but are not limited to:

* Fixing RCA items from the incident
* Improving observability
* Improving unit test coverage
* Adding integration tests
* Improving service level monitoring
* Improving symmetry of pre-production environments
* Improving the [GitLab Performance Tool](https://gitlab.com/gitlab-org/quality/performance)
* Adding mock data to tests or environments
* Making process improvements
* Populating their backlog with further reliability work
* Security work

Any work kicked off during this period must be completed, even if it takes longer than the duration of the FCL. Any work directly related to the incident should be kicked off and completed even if the FCL is over. Work paused due to the FCL should be the priority to resume after the FCL is over.

The stable counterpart from Infrastructure will be available to review and consult on the work plan.

This is in effect as of September 2, 2021 (retroactive) for six months. R&D leadership will evaluate Mar 1, 2022 and decide whether to continue, modify, or cancel the process (already scheduled).
