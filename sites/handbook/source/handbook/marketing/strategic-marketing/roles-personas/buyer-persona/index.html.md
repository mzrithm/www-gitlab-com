---
layout: handbook-page-toc
title: "Buyer Personas"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


![IT Orgchart](/handbook/marketing/strategic-marketing/images/IT-Org-v2.png)


### Alex - the Application Development Manager

#### Who - Demographics & Environment

* **Background:**  
   *  Front line leader who in first 'management' role.  
   *  Evolved from individual contributor to a team lead on projects, to full time 'management' role.  

* **Demographics:**  (need more data)
   *  Typically male, with ~6-10 years of experience.
   *  Income range depends on industry and location.
   *  Mostly major metropolitan areas world-wide with development teams.

* **Identifiers** - Demeanor? Communication preferences?:  
   * Highly technical, and only one step removed from an individual contributor.  
   * Get into the details more than his leaders (directors and VPs).  
   * When evaluating solutions, likely to explore product demos and dig into the technical details.

* **Company** - Market segment/industry? Size? On-site, distributed? Full Remote?  
    * Works in major metropolitan areas around the world.
    * Often managing a development teams comprised of employees and external firms (on-site, distributed or remote).

* **Team Details:** Size? Development Methodology? DevOps adoption path?  
    * Manages team of 8 to 12 developers using Agile to plan and manage their work.  
    * In the process of automating development processes for building and testing their application(DevOps).

* **Influencers- where do they learn:**
    * Reads SD Times, DevOps.com, Hackernoon, StackOverflow, and others.  
    * Frequently checks out CIO Magazine for leadership and bigger picture challenges.
    * Because his organization is focused on cloud transformation, he attends AWS conferences and is actively following key webinars that help him learn about how he can better apply technology to meet his business's demands.

#### What - Motivations

* **Goals/motivations:**  - His goals are tied to several dimensions.  
    * *Business Satisfaction* is a key goal, as he manages the IT-Business relationship for the team.
    * Working with the business to prioritize the backlog and determine what user stories will be in the upcoming sprint.  
    * Managing his budget and ensuring that his team has the right skills and overall team engagement.   
    * Reinforce the importance of key SDLC and process tasks ranging form quality, documenting code reviews, security, and accurate time tracking.

* **Challenges:** - Alex is looking for help solving his challenges:
    * He never has enough resources (time, people, or infrastructure) to deliver on all the competing requests.
    * Balancing new features against technical debt acrued during prior development.
    * Balancing scope, budget, resources, and expectations on a daily basis.
    * Lack of accurate and real time status of individual work, leading to him having to disrupt team members with questions.
    * Persuading other stakeholders to consider and purchase new tools.
    * Understanding potential negatives of adding new tools to existing toolsets.

* **What can we do to help them achieve goals and overcome their challenges?**
1. Streamline team collaboration and increase team productivity to meet business needs and expectations.
1. Simplify tracking and monitoring of their teams work. With GitLab, they are able to see the actual status of issues and merge requests.
1. Productivity and performance metrics help them improve their teams performance.
1. Compliance and reporting.
2. Make it easy for them understand how GitLab solves their challenges, and how to adopt with minimal risk/disruption.
3. Give them compelling content to pursuade others in their organization to choose GitLab.

* **Why do we want to connect** Depending on the size of the company where he works, his role in the tool selection process will vary.  
- **SMB**: Likely responsible for the tool selection decision and has budget authority for his team's tools.  
- **MidSize**: Significant influence over the overall tool selection process, however may not be the final purchasing authority.
- **Large** (enterprise): In a large enterprise, there are multiple development teams and development managers.   In *some* situations, they may have autonomy to select tools for their team, however it is more common for teams to have tool purchases centrally managed, and the **Dev Manager** will play a key role in *recommending* a solution, but will not be the final buyer.


* Real Quotes - Examples of things they commonly say about their goals and challenges.
(Need to gather data)

* **Common Objections** - Why wouldn't they buy us?
1. It's not their 'corporate standard', they have other standard tools, and it's above their pay grade to change.
1. They need to justify the effort to convert from an existing legacy tool to GitLab.  The purchase is only one part of their business case, and they need to justify the labor as much as the OPEX they are spending.
1. They are not responsible for the full tool chain, in fact, they may not 'own' any of the end to end tools
1. They don't have a large tool budget, it's a shared function or managed higher up in the organization.

#### Why - Messaging

* **Discover Questions** - questions to ask
1. "Tell me about the business your team supports" - Learn about the underlying business system and technology they support.
1. "What challenges are you facing with keeping up with business requests?"  - Almost ALL development managers don't have enough resources to keep up and are always trying to prioritize what next.
1. "How are you measuring success, are there strategic goals to accelerate delivery or improve efficiency?" - What kind of challenges are they facing
Note: need to add a few more from Command of the Message

* **Messaging** - How describe our product and value drivers to this persona?
1. GitLab is a key to unlocking the true potential of your team to help you meet the needs of your business partners.  GitLab helps teams become more efficient and effective and stop wasting time waiting.  It is easy for developers to maintain context of work from requirements to code to testing and even deployment.  
1. Improve visibility and insight so you can know the true status of deliverables without disrupting your team.
1. Streamline developer on boarding and bring new team members up to speed faster and more efficiently.

* **Elevator pitch** - What's our 1-2 sentence pitch to this persona?
- Help your team become more effective and efficient. Enable them to collaborate and accelerate application delivery, eliminating wasted time switching context from one tool to another, **and** get data and insight into how the team is performing.


---

### Dakota - the Application Development Director
#### Who - Demographics & Environment

* **Background:**  Dakota is a key IT leader who manages and leads several teams of developers supporting a specific set of business applications.  She has both technical and business skills and as a manager she's focused on delivering business innovation.

* **Demographics:**  (need more data) Typically male, with ~9-15 years of experience. Income range depends on industry and location. They can be found in all most all major metropolitan areas where there are development teams.

* **Identifiers** - Demeanor? Communication preferences?:  Dakota is removed from day to day technical details and focuses her days on planning future work, team skill development, business relationships, and overall team performance. She is concerned about cost and feasibility, managing risk, and leading organizational change. When evaluating solutions, she is more likely to focus on value over specific features.  She will rely on her team to dig into the technical details and demos, etc.

* **Company** - Market segment/industry? Size? On-site, distributed? Full Remote?  Dakota can work just about anywhere, in most major metropolitan areas around the world.   She will typically be in mid-size to large organizations where they have a significant IT and Developer team. There are opportunities for remote work, and often she's managing a blended team of developers who are both her employees and developers from external firms.

* **Team Details:** Size? Development Methodology? DevOps adoption path?  She manages 3 to 5 managers, each who has from 8 to 12 team members. They are in the process of automating development processes for building and testing their application(DevOps). In pre-IPO, large, late start-up organizations, Dakota may have up to 15 direct reports ranging from technical resources like Software Development Leads or Archictects to DevOps managers or Software Development Leadership.

* **Influencers- where do they learn:** Dakota reads CIO Magazine, Gartner, Forrester and other analyst reports. She attends both industry specific conferences and also strategic technology conferences such as Forrester, Gartner, and others. She is actively looking for ideas and insight into how to improve the efficiency and effectiveness of her teams.

#### What - Motivations

* **Goals/motivations:**  Her top goal is *Business Satisfaction*, and regardless of the technology, she wants to deliver consistent and predictable business results. She has had a long relationship with her business unit and feels as if she is an extension their team. She is responsible for the IT strategy for her business unit and is looking at ways to improve the business value of her systems.  Through her managers, she is responsible for growing and improving her team and how they improve their delivery effectiveness. She manages her team's budget, ensuring that they have the tools, training, and resources to be successfully develop and deliver the business value. Dakota is tasked with finding the right tools for the  tasks in her in teams and often will spend more money to get the results of team effectiveness, accomplish results, or deliver the job on time.

* **Challenges:** She balances her time between strategic planning with her business partners, and also resolving organizational issues and roadblocks her teams are facing. She develops organizational strategies and plans to secure budget and resources for her team.  She is often balancing budget across the business and different levels, where she is creating jutification between spending on new tools or people from the same budget.    

* **What can we do to help them achieve goals and overcome their challenges?**
1. Streamline team collaboration and throughput, helping their team deliver more features for the business
1. Simplify their job of tracking and monitoring progress of their team working on development.  With GitLab, they are able to see the actual status of issues and merge requests.
1. Successfully accomplish as many tasks across the Development Lifecycle with completeness to support team effectiveness, not just just efficiency. Delivering on the promise of the DevOps Platform. 
1. Productivity and performance metrics help them improve their teams performance
1. Compliance and reporting

* **Why do we want to connect** Depending on the size of the company where she works, her role in the tool selection process will vary.  
- **SMB**: Core decision maker, tool selector, researcher, and final purchasing authority for team. 
- **MidSize**: Significant influence over the overall tool selection process, and probably will have final purchasing authority.
- **Large** (enterprise): In a large enterprise, there are multiple development teams and development managers. In *some* situations, they may have autonomy to select tools for their teams, however it is more common for tools to be centrally managed, and the **Dev Director** will play a vital and central role in *endorsing and recommending* a solution, where they are the champion that drives the final selection and recommendation.  

* Real Quotes - Examples of things they commonly say about their goals and challenges.

"Time is a big deal. It’s our most valuable asset." - Director of Engineering, 2000+ Employees in Technology Industry

"Tools that slow us down, but allow us to make better decisions, provide a lot of value." - Application Development Director, 500+ Employees in Education Industry

* **Common Objections** - Why wouldn't they buy us?
1. It's not their 'corporate standard', they have other standard tools, and they are not willing to take the risk to promote a change.
1. They need to justify the effort to convert from an existing legacy tool to GitLab. The purchase is only one part of their business case, and they need to justify the labor as much as the OPEX they are spending.
1. They are not responsible for the full tool chain, in fact, they may not 'own' any of the end to end tools.
1. They don't have a large tool budget, it's a shared function or managed higher up in the organization.
1. They can only approve tools up to $50,000 annually or some other relatively low cap on spend. 

#### Why - Messaging

* **Discover Questions** - questions to ask
1. "Tell me about the business your teams support" - Learn about the underlying business system and technology they support.
1. "What challenges are you facing with keeping up with business requests?" - Almost ALL development managers don't have enough resources to keep up and are always trying to prioritize what next.
1. "How are you measuring success? Are your strategic goals to accelerate delivery or improve efficiency?" - What kind of challenges are they facing
Note: need to add a few more from Command of the Message

* **Messaging** - How describe our product and value drivers to this persona?
1. GitLab is a key to unlocking the true potential of your team to help you meet the needs of your business partners.  GitLab helps teams become more efficient and effective and stop wasting time waiting.  It is easy for developers to maintain context of work from requirements to code to testing and even deployment.  
1. Improve visibility and insight so you can know the true status of deliverables without disrupting your team.
1. Streamline developer onboarding and bring new team members up to speed faster and more efficiently.

* **Elevator pitch** - What's our 1-2 sentence pitch to this persona?
- Help your team become more effective and efficient. Enable them to collaborate and accelerate application delivery, eliminate wasted time switching context from one tool to another, **and** get data and insight into how the team is performing.

### Erin - the Application Development Executive (VP, etc.)

#### Who - Demographics & Environment

* **Background:**  Erin is senior IT and development executive, who leads a large team of developers who build and support a line of business. Her career has spanned a number of roles in different organizations, and is focused on strategy and ensuring the team has resources to succeed.

* **Demographics:**  (need more data) Typically male, with ~15-20 years of experience. Income range depends on industry and location. They can be found in all most all major metropolitan areas where there are development teams.

* **Identifiers** - Demeanor? Communication preferences?:  Erin is a strategic leader focused on business challenges and the big picture.  Her time is valuable and wants people to be direct and to the point.  She's not interested in all the technical details, but rather needs to understand the value, the risks, and how the decisions she is making today will help the business in the long term.

* **Company** - Market segment/industry? Size? On-site, distributed? Full Remote?  Erin is an application development executive, and is typically found in larger enterprises where they have large development teams (hundreds of developers). Therefore, Erin can find in most major metropolitan areas around the world where there are large enterprises.  She frequently manages a blended team of leaders (managers and directors) and developers who are in multiple locations. .

* **Team Details:** Size? Development Methodology? DevOps adoption path?  She directly manages a team of 4-5 directors who each manages teams of developers led by their respective managers. Depending on the mixture of legacy applications and systems, they have a wide variety of methodologies in play, but in general, their goal is to streamline delivery to be more efficient and predictable and are  in the process of automating development processes for building and testing their application(DevOps).

* **Influencers- where do they learn:** Erin reads Forbes, CIO magazine, Bloomberg, and other specific business publications.  She stays current on specific trends in her industry, as she is a Business Leader who happens to be responsible for technology. She rarely reads technical blogs or reviews. She attends business focused technology conferences in order to learn and also uses analyst firms like Forrester and Gartner to understand key technology trends and their business implications.

#### What - Motivations

* **Goals/motivations:**  Her top goal is predictable *Business Results*.  As a business leader, she partners with other executives to set strategy and goals for the business and then leads her team to help deliver the required technology.  She's focused on leading, growing, and improving her team to be able to deliver results. Her results are measured on her team's ability to deliver results according to the strategic plans (budget, schedule, and risk) are KPIs for her.

* **Challenges:** She is removed from the day to day work her teams do, and visibility is a major challenge.  She relies on status reports, dashboards, and other updates to get insight into how things are going, and too often learns of problems too late. She spends most of her time working on strategic alignment and planning with other executives, dealing with problems and exceptions when they are escalated to her level.  She develops organizational strategies and plans to secure budget and resources for her team.   

* **What can we do to help them achieve goals and overcome their challenges?**
1. Visibility into execution - connecting strategic vision through to actual delivery
1. Team satisfaction, as they will be able to collaborate and deliver more efficiently
1. Productivity and performance metrics help them improve their teams performance
1. Compliance and reporting

* **Why do we want to connect** Depending on the size of the company where she works, her role in the tool selection process will vary.  
- **SMB**: NA
- **MidSize**: NA
- **Large** (enterprise): In a large enterprise, the App Dev executive will have significant authority and influence over the budget. While they will **NOT** make the technical decision, they **WILL** make the business decision. They will depend on their *Directors*, *managers*, and key staff members in evaluating and selecting tools and solutions from the market.

* **Common Objections** - Why wouldn't they buy us?
1. The cost and effort to convert from an existing legacy tool to GitLab is too high. The purchase is only one part of their business case, and they need to justify the labor as much as the OPEX they are spending.
1. They don't have a large tool budget, it's a shared function or managed higher up in the organization.
1. Concerned about business risk, GitLab is an unknown compared to other more established vendors.

#### Why - Messaging

* **Discover Questions** - questions to ask
1. "How do you see technology disrupting your business?" - Learn about the underlying business system and technology they support.
1. "What challenges are you facing with keeping up with business requests?" - Almost ALL development managers don't have enough resources to keep up and are always trying to prioritize what next.
1. "How are you measuring success? Are your strategic goals to accelerate delivery or improve efficiency?" - What kind of challenges are they facing
Note: need to add a few more from Command of the Message

* **Messaging** - How describe our product and value drivers to this persona?
1. GitLab is a key to unlocking the true potential of your team to help you meet the needs of your business partners.  GitLab helps teams become more efficient and effective and stop wasting time waiting.  GitLab makes it easier for developers to maintain context of work from requirements to code to testing and even deployment.  
1. Improve visibility and insight so you can know the true status of deliverables without disrupting your team.
1. Streamline developer on boarding and bring new team members up to speed faster and more efficiently.
1. Simplify compliance, auditing, and security with by giving your developers a common work environment where they can collaborate and deliver.

* **Elevator pitch** - What's our 1-2 sentence pitch to this persona?
- Help your team become more effective and efficient. Enable them to collaborate and accelerate application delivery, eliminating wasted time switching context from one tool to another, **and** get data and insight into how the team is performing.


### Skyler - the Chief Information Security Officer

#### Who - Demographics & Environment

* **Background:**  Skyler is a seasoned Information Security & Risk Management expert with strong leadership and business skills - required to communicate complex security jargon outside of the IT and Security departments and to ensure the department has adequate funding and resources to secure the company. She leads a small team of security experts - and works closely with the IT, Network and Desktop teams to ensure security in those respective areas.

* **Demographics:**  Typically male, with ~15-20 years of experience. Income range depends on industry and location. Typically found in metropolitan areas and have a bachelors or masters degree in Information Security or Information Management ([source](https://www.varonis.com/blog/ciso-skills/))

* **Identifiers** - Demeanor? Communication preferences?:  Skyler is a business leader focused on preventing security issues from occurring, but understanding that it is not possible to prevent all of it. He/She understands technical and business jargons fluently and is able to articulate the value and cost of security/compliance to both business & technology audience with ease. She is proficient working with matrixed organizations, can maintain a calm demeanor during crisis and has expertise in working with cross functional teams and building/maintaining relationships

* **Company** - Market segment/industry? Size? On-site, distributed? Full Remote? In small and mid sized organizations, Skyler’s role of CISO is normally covered by the CIO. When the organization grows and has increased security requirements which the CIO cannot manage anymore, Skyler’s role of CISO becomes a separate role.  She has a small team of her own with cross team dependencies

* **Team Details:** Size? Development Methodology? DevOps adoption path?  Skyler manages a small team of security experts and builds security champions within other teams like Dev, IT, Network & Desktop who handle the day to day security issues. Typically, Skyler will have her own set of tools for application security which is frequently disconnected from the DevOps path. As a result, Skyler and her team are always playing catch-up leading to slower product release timelines or have tools that are fairly thorough but take much longer to provide security results, thereby delaying product releases. In some organizations, Skyler reports into the CIO and in some others reports into the CFO.

* **Influencers- where do they learn:** Skyler  reads Forbes, CIO magazine, Bloomberg, and other specific business publications and CISO Mag, Info Security Magazine for security related insights. He/She stays current on specific trends in her industry, as she is a Business Leader who happens to be responsible for technology. She attends business & technology conferences like RSA, SHMOOCON, OWASP, Info Sec World, Gartner Security Summits etc in order to learn trends in information security and also uses analyst firms like Forrester and Gartner to understand key technology trends and their business implications.

#### What - Motivations

* **Goals/motivations:**  Skyler’s top goal is to *manage security risk and compliance*. Her team has a goal to be able to integrate better into the DevOps process, automate security processes, catch security issues faster & earlier in the cycle to be able to release faster. Her team also aims to build security champions in cross functional teams and limit involvement of security experts for critical path only. She and her team’s results are measured through security related KPIs and the cost of compliance

* **Challenges:** Typically shortstaffed as security experts are hard to come by and expensive. More often than not, there are multiple different tools used for security and achieving a consolidated view of security is a pipedream for most CISOs. Distributed data also implies limited ability to show progress on KPIs and hence challenges in retaining or securing additional budgets.

* **What can we do to help them achieve goals and overcome their challenges?**
1. Shift security left - detect security issues earlier and faster
1. Better collaboration and communication across Development and Security teams
1. Security experts freed up to focus on high value work
1. Auditing, compliance and reporting

* **Why do we want to connect** Depending on the size of the company where she works, her role in the tool selection process will vary.  
  - **SMB**: In SMB companies, there is no security executive role. Developers or Development managers double up to cover basic security checks and will make the technical decision and purchase decision will be owned by the founders / owners.
  - **MidSize**: In mid sized companies, CISO or the security function will not be a formal role/team & will be either outsourced,  covered by the CIO themselves OR a chief security advisor. While the chief security advisor or the outsourced security team will not make the business decision of purchase, they will make the technical decision and influence the purchase. The CIO will depend on key staff members in evaluating and recommending the purchase, while the budget will be owned by the CIO.
  - **Large** (enterprise): In a large enterprise, the CISO will have significant authority and influence over the budget. The CISOs team will make the business & technical decision. Based on where the CISO reports into - their objectives may vary. A CISO reporting into CFO may have compliance / auditing & reporting as a goal while a CISO reporting into CIO may have application security as a goal

* **Common Objections** - Why wouldn't they buy us?
1. The cost and effort to convert from an existing security toolset is too high
1. Already invested heavily in a security toolset, looks like we will need both for the short - medium term
1. The security capability is not as thorough as their existing toolset
1. GitLab is not known for its security capabilities
1. Concerned about business risk, GitLab is an unknown compared to other more established vendors
1. The price jump to derive value from security is too high

#### Why - Messaging

* **Discover Questions** - questions to ask
1. "How is security testing currently done in your organization??" - Learn about the underlying system, technology and number of tools required to achieve similar outcomes.
1. "What is the current process for finding vulnerabilities, dependencies and compliance adherence in your organization??" - Most often, security is a disconnected process. Done as an afterthought and after the development process is almost complete - leaving little or no time for security to identify & developers to fix the issues
1. "What is the cost of non compliance to your organization?" - Different industries have different needs to adhere to compliance. Cost of non compliance is typically high


* **Messaging** - How describe our product and value drivers to this persona?
1. GitLab embeds security right into your MR pipeline and provides consolidated results across different types of security testing making it easier to identify and fix security vulnerabilities earlier in the release cycle
1. GitLab helps to improve visibility and insight of your security vulnerabilities across projects - so you can know a consolidated, true status of deliverables without disrupting your team or referring to multiple other tools to get an insight
1. GitLab allows you to apply security policies and gating within the MR pipeline itself allowing you to review/approve security exceptions before the code is merged
1. GitLab allows you to simplify compliance, auditing, and security by giving your developers & security champions  a common work environment where they can collaborate and deliver.
1. GitLab helps you minimize / eliminate the licenses of "n" other security tools with a single tool covering various areas of your application security.

* **Elevator pitch** - What's our 1-2 sentence pitch to this persona?
- Embed security within your Development process and allow developers own the security vulnerabilities introduced in the code within the CI/CD pipeline rather than as an afterthought - minimizing time to detect, fix and release the product without context switching to different tasks or tools and receive a consolidated view of application security status
- Streamline auditing by recording every change and maintaining a single thread that contains the full audit log of every decision and action - making audit compliance a breeze


### Kennedy - the Infrastructure Engineering Director
This role could also be defined as **Infrastructure Operations** in some organizations. However, in most organizations, Infrastructure Operations deals with managing the health and performance of the deployed application, incident management and triage.

#### Who - Demographics & Environment

* **Background**
  * Experience in building and scaling highly available operations
  * Evolved from IT administration and IT infrastructure management

* **Demographics** (need more data)
  * Typically male, with over 10 years of experience
  * Income range depends on industry and location.
  * Mostly colocated with engineering operations

* **Identifiers** - Demeanor? Communication preferences?:
  * Bachelor’s Degree computer science, information systems or related field
  * Highly technical, with experience in cloud centric architectures, migrating to cloud
  * Concerned about cost and feasibility, managing risk and compliance, and site reliability practices

* **Company** - Market segment/industry? Size? On-site, distributed? Full Remote?
  * Mostly colocated with engineering operations, although can be located anywhere in case of hosted / cloud centric infrastructure
  * Can be part of an outsourced entity for infrastructure operations as well

* **Team Details** Size? Development Methodology? DevOps adoption path?
  * Has a team of managers with specializations around database, middleware, virtualization, container management, and cloud focussed.
  * Team has a focus to automate deployments, evangelize infrastructure best practices and resource allocation across the entire company
  * Reports into the CIO organization

* **Influencers** where do they learn:
  * Heavily influenced by best practices from peers
  * Attends industry conferences by vendors like Amazon, Google, Microsoft to get insights into the latest advancements in cloud architectures, automation and artificial intelligence
  * Also influenced by analyst reports like Gartner and Forrester to determine industry trends and vendor selection
  * They like to stay up to date with certifications like ITIL, CCNA, AWS CSA, MCDC etc
  * Reads CIO magazine, MIT Tech Review, DevOps.com and other specific business publications


#### What - Motivations

* **Goals/motivations**
  * Top motivation is to balance costs of operations vs application performance and availability without compromising compliance or security.
  * Lead and plan the day-to-day execution of infrastructure and cloud operations, policies, processes to support corporate strategy via inhouse staff or outsourced staff
  * Implement best practice in deployment performance monitoring and alerting process to ensure timely and effective resolution
  * Lead migration of existing infrastructure services from physical data centers to cloud
  * Work with application developers to drive technical innovation and efficiency in infrastructure operations through automation tools and self-repair utilities
  * Maintain an up to date inventory (database) of available application and infrastructure and manage change adequately

* **Challenges**
  * Balancing time between strategic activities and day-to-day firedrills
  * Organizational priorities misaligned with operational challenges
  * Constantly justifying build v/s buy v/s outsource decisions
  * Achieving risk and compliance across the organization

* **What can we do to help them achieve goals and overcome their challenges?**
  * Collaborate better to align dev, ops priorities
  * Automation of application and infrastructure deployment to help prioritize strategic initiatives
  * Centralized change management, compliance monitoring and reporting

* **Why do we want to connect?**  Depending on the size of the company, role in the tool selection process will vary.
  * SMB: May not have a separate role for infrastructure engineering, IT / Dev managers / directors would double up for infrastructure engineering
  * MidSize: In mid sized companies, infrastructure engineering may be either outsourced, covered by the CIO themselves OR engineering managers. Outsourced team may not make a purchase decision, but will influence technical decision on the purchase. The CIO will depend on key staff members in evaluating and recommending the purchase, while the budget will be owned by the CIO.
  * Large (enterprise): In a large enterprise, there will be a dedicated team for application and infrastructure engineering - enabling self service operations of  infrastructure for development teams, making infrastructure decisions to optimize cost and performance, managing post deployment performance of the deployed  application. The director will have influence in determining tools to be used for application and infrastructure deployment and management.
* **Real Quotes** - Examples of things they commonly say about their goals and challenges. (Need to gather data)
* **Common Objections** - Why wouldn't they buy us?
  * Prefer a specialized tool rather than a generic tool that does other things as well
  * Need to justify the investment in existing tools while also requiring yet another toolset
  * They do not own the end to end process and hence GitLab may be expensive for their specialized usecase
  * It’s not their corporate standard tool and they believe it is an uphill battle to influence change

#### Why - Messaging

* **Discovery Questions** - questions to ask
  * “How complex and expensive is managing and supporting the development lifecycle and tool chain?“ - Are they feeling pressure to manage cost and complexity
  * “Tell me how often do you deploy to production” - Learn about their pace of development and deployment
  * “What are the challenges you face in trying to speed up this process” - Learn about underlying issues, lack of collaboration, lack of automation
  * “Can you tell me about the last time you encountered issues in production?" - Learn about their deployment strategies, are they sophisticated with lots of automation and monitoring, or do production issues become full-blown escalations? 
  * “How are you able to manage and track change” Learn about how they do compliance and audit management
  * “What are the metrics you use to determine your team’s success” - Learn about efficience,

* **Messaging** - How describe our product and value drivers to this persona?
  * Lower costs and simplify your development processes. GitLab is a single application that can reduce license and management overhead while helping teams deliver more reliable secure code.
  * GitLab helps you automate your application and infrastructure deployment - enabling you to keep up with the development team pace and deploy to production faster
  * Achieve deployment assurance via various deployment strategies and performance monitoring to ensure you are able to catch production issues before it becomes a wide spread escalation
  * GitLab allows you to simplify compliance, auditing, and risk by giving a common work environment where they can collaborate and deliver.
  * GitLab helps you to maintain environment consistency by allowing you to codify infrastructure and policies - enabling you to create a consistent and repeatable infrastructure every time

* **Elevator pitch** - What's our 1-2 sentence pitch to this persona?
  * Automate your application and infrastructure deployment process to deliver a consistent and repeatable environment for your applications.
  * Streamline auditing by recording every change and maintaining a single thread that contains the full audit log of every decision and action - making audit compliance a breeze


### Casey - the Release and Change Management Director

#### Who - Demographics & Environment

* **Background:**
  * Experience in managing releases from concept to delivery
  * Experience in coordinating across multiple teams, identifying and mitigating release risks
  * Evolved from a developer role to program / release management

* **Demographics:**
  * Male or female, with over 10 years of release management experience
  * Income range depends on industry and location
  * Mostly colocated with engineering teams

* **Identifiers:** - Demeanor? Communication preferences?
  * Bachelor’s degree in computer science, electronics, information systems or related field
  * Highly process focussed, with experience in SDLC models
  * Demonstrated ability to deliver complex projects on time and in scope

* **Company - Market segment/industry? Size? On-site, distributed? Full Remote?**
  * Mostly colocated with engineering teams, not outsourced if engineering is not outsourced
  * In small and medium sized organizations, this role is covered by the engineering manager / project manager. As team grows, release management is a separately managed function with a representative in the development team.

* **Team Details:** Size? Development Methodology? DevOps adoption path?
  * TBD
  * Release managers now are required to have experience in DevOps and Agile practices

* **Influencers- where do they learn:**
  * Most of the learning comes from peer networks
  * Release managers are process experts - so they like to stay up to date with process certifications like SAFe, Agile, Project Management, ITIL, governance etc
  * Reads Forbes, CIO magazine, Bloomberg, and other specific business publications
  * Refers to analyst trends and analyst reports for decision making

#### What - Motivations

* **Goals/motivations:**
  * Lead on-time delivery of high quality Software releases
  * Manage Releases from concept to delivery
  * Develop schedules and actively manages content of Software releases
  * Identifying project risks and manage mitigations
  * Deliver detailed status on program health at NPI and Software program review meetings
  * Establish efficient change management controls
* **Challenges:**
  * Implementing change management across various parts of the organization
  * Achieving alignment on tools and priorities of release management activities with development and operations activities

* **What can we do to help them achieve goals and overcome their challenges?**
  * Single tool to collaborate release management activities with dev and ops - easier to collaborate and align priorities
  * All change across the workflow is tracked in a single data store - making it easier to achieve traceability, compliance and reporting
  * Establish centralized change management process

* **Why do we want to connect?**  Depending on the size of the company, role in the tool selection process will vary
  * **SMB and Midsized**: Normally engineering manager / Project manager doubles up as release manager. These will report into the CIO who will depend on them for the evaluation, budget is owned by the CIO
  * **Large (enterprise)**: In a large enterprise, there is a dedicated centralized release management and change management practice. The director will be the buyer for tools to be used in release and change management.

* **Real Quotes** - Examples of things they commonly say about their goals and challenges. (Need to gather data)

* **Common Objections - Why wouldn't they buy us?**
  * Development teams have already made an investment in a dev tool that already supports release management
  * Prefer a specialized tool for release and change management
  * Organizational policy to use other tools and it is a challenge to influence change
  * Don’t own the end to end process - GitLab is expensive only for release management

#### Why - Messaging

* **Discovery Questions - questions to ask**
  * “Tell me how do you plan and schedule your releases” - Learn about current toolset being used, if it is a separate tool from source control & CI/CD
  * “What are the challenges you face in your release processes” - Learn about underlying issues, lack of collaboration, lack of traceability
  * “Tell me how you achieve traceability to requirements” - Learn about their change management and compliance policies
  * “Tell me how you manage access control across various tools” - Learn about their challenges in setting up approval rules, managing change

* **Messaging - How describe our product and value drivers to this persona?**
  * GitLab enables you to coordinate across cross functional product and business teams to identify and mitigate release risks from a single tool
  * GitLab helps you plan complex releases using the same tool used by your developers and infrastructure delivery teams - enabling easy cross team coordination
  * Changes, secrets & credentials can be managed centrally - allowing you to achieve efficient change management processes and audit compliance

* **Elevator pitch - What's our 1-2 sentence pitch to this persona?**
  * Streamline release management of complex projects using a single application that facilitates cross team coordination, maintains traceability to requirements and maintains an audit log of every decision and change - making change management and audit compliance easy
