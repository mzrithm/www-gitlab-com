---
layout: handbook-page-toc
title: "Education Evangelism"
description: "We build DevOps awareness with GitLab by creating engaging and relevant content to connect with the Education community."

---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

## <i class="fa fa-map-marker" aria-hidden="true"></i> Mission

To support, grow, and engage the GitLab Education community through collaboration, content, and conversations.

### <i class="fa fa-map-o" aria-hidden="true"></i> Strategy
Education evangelism is a specialty of developer relations aimed at evangelizing DevOps and GitLab at educational institutions around the globe.

Our stakeholder community includes students, faculty, staff, administrators, and learners of any origin. The GitLab Education Evangelist team seeks to increase awareness, inform, and connect with community members through a variety of mediums. We also are visible power users of GitLab and DevOps applications. We strive to be a resource for all learners and, in accordance with GitLab's [Diversity, Inclusion, and Belonging Values](/company/culture/inclusion/), we seek to increase outreach to under-represented and under-estimated people in the education community.

For this reason, we choose to have a team with credibility from real-world educational and technical experience.  Additionally, we enable others to tell **their** authentic stories - engineers, open source community members, and GitLab's diverse set of customers.  For those reasons, we chose the title of [`Evangelist`](https://www.merriam-webster.com/dictionary/evangelist) as defined as "an enthusiastic advocate."


## What we do
[Social media](/handbook/marketing/community-relations/education-program/education-evangelism/#social-media){:.btn .btn-purple-inv}
[Content creation](/handbook/marketing/community-relations/education-program/education-evangelism/#content-creation){:.btn .btn-purple-inv}
[Presentations and events](/handbook/marketing/community-relations/education-program/education-evangelism/#presentations-and-events){:.btn .btn-purple-inv}
[Student Spotlight Program](/handbook/marketing/community-relations/education-program/education-evangelism/#student-spotlights-program){:.btn .btn-purple-inv}



The GitLab educational evangelists are technical evangelists with a background in or enthusiasm for education and students of all levels.  Educational evangelists connect, engage, and create technical content for our education community.

There are specific goals for the team:

1. **Create and Curate Content:** Create and curate content to enable faculty, students, and staff to adopt and be successful with GitLab. This includes blog posts, webinars, videos, eLearning, courses, and best practices.
1. **Connect and Engage:** Evangelize the GitLab for Education Program. Connect and engage with Education Program Members and the wider community through our forum, social media posts, twitch steams, student spotlights, coffee chats, and at events.  Gather feedback from the community.


### Social media
The GitLab for Education team builds thought leadership, connects with our community, and increases our reach on social media.  See the [Education Evangelism on Social Media](/handbook/marketing/community-relations/education-program/education-evangelism/social-media/) handbook page to learn more about our social media strategy.


### Content creation
We build out content to help educate developers around best practices related to Education, GitLab, and DevOps. Content includes presentations, demos, course materials, best practices, workshops, livestreams, blog posts, and social media engagements.

Content is especially valuable when connected between mediums. Consider making a video version of a blog, or livestreaming the creation of a code demo or lesson plan.

Livestreaming guidelines can be found on our [Twitch handbook page](/handbook/marketing/community-relations/education-program/education-evangelism/twitch/)


#### Blog Posts
Content will either be published on the [GitLab Blog](/blog/), where all our blogposts include the `education-program` [postType](/handbook/marketing/blog/#post-type) in their [front matter](/handbook/marketing/blog/#frontmatter) for proper tracking.

If the blog post is not featured on the GitLab filtered blog, posts will publish on the GitLab group[Dev.to](https://dev.to/gitlab)


### Presentations and events
Participation in events such as conferences and conventions are an important part of Evangelism. In the case of Education Evangelism, both tech and education events are important to take part in. An Education Evangelist seeks out relevant organizations and events to participate in as part of their role. As events and organizations to consider are found we will add them to an issue in the [Outreach project](https://gitlab.com/gitlab-com/marketing/community-relations/education-program/outreach/).

When preparing for presentations and talks, post your slides to the [Developer Evangelism](https://gitlab.slack.com/archives/CMELFQS4B) or [Community Relations](https://gitlab.slack.com/archives/C0R04UMT9) slack channel for feedback on slides. This ensures that the most current messaging is being used. It's also good for a sanity check and quick edit on the content. Another good way to review for a speaking engagement is to post a zoom room to the [speaking channel](https://gitlab.slack.com/archives/CCGD905AR) and ask for people to join a practice session. 

Education Evangelists should consider being a part of the [Speakers Bureau.](https://about.gitlab.com/speakers/)


#### YouTube
Recordings from past workshops and other team activities can be found on the [GitLab Unfiltered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A) channel on YouTube.


#### CFPs
We directly contribute to the wider community by speaking at education and tech conferences. If you've come across a conference you feel is valuable to the Education Team, please open an issue in the [Outreach project](https://gitlab.com/gitlab-com/marketing/community-relations/education-program/outreach/) and use the `edu-conference` label to suggest a conference for attendance.


### Student Spotlight Program
The Student Spotlight Program provides an opportunity for students, faculty, and staff to feature their work on GitLab! The aim of the program is two-fold. First, we hope to inspire others to join the program by showcasing some of the exciting work our members are doing. Secondly, we want to connect and build relationships with institutions already using GitLab for teaching and learning.

Students, faculty, or staff can apply to be highlighted in the Students Spotlights Program by filling out the [application form](https://docs.google.com/forms/d/e/1FAIpQLSdHzG0IEDw6VlUQDqWsweNRDIdM2HQpoBH-t2OzK1m0_SMeiQ/viewform). Our team will review the applications and selected projects will be featured via a video-recorded interview with the students and professors involved. Interviews and project links will be featured on the GitLab Unfiltered YouTube channel and the GitLab for Education main webpage.


## <i class="fa fa-code-fork" aria-hidden="true"></i> How we work

### Issues
All of our work is stored as issues in the [Education Program Group](https://gitlab.com/gitlab-com/marketing/community-relations/education-program). Issues are stored in projects. A list of projects and descriptions can be [found here.](https://gitlab.com/gitlab-com/marketing/community-relations/education-program)


#### Issue labels

| **Label** | **Description** |
| ---------- | ----------- |
| `edu-twitch` | This is for issues related to Twitch streams |
| `edu-evangelism` | This is a general label for Education Evangelist related issues |
| `edu-conference` | Label used for conference activity planning |
| `student-spotlights` |  For issues relating to the student spotlights |


## Impressions
We measure the impact of our content and outreach via impressions. We plan our activities for the quarter with an epic named  "Outreach Formats and Content FYXX QX".  This includes articles, videos, talks, and any shareable content. Content creators have the option of adding a comment for each piece of content or creating an issue, however, [consider an issue first.](https://about.gitlab.com/handbook/marketing/project-management-guidelines/issues/)

A table should be created in the description of the epic. The columns are `Type of Outreach`, `Views`, `Publication Date`, and `Working Issue`.

### Metrics Collection and Analysis
The GitLab for Education Team tracks our impressions on the [Education Program Impression Dashboard](https://datastudio.google.com/u/0/reporting/6160877c-c921-4554-965f-f7a7eb7c36cd/page/YsgmB). All outreach should be entered into the [Education Asset Inventory Google Sheet](https://docs.google.com/spreadsheets/d/1k1ci9BPEDDb_CxrR9-uQ_YQD_UXBMDeAU-KTpfQfHD4/edit#gid=0) in order to appear in the dashboard. Directions for entering each publication are in the `Read Me` tab of the spreadsheet.

### Best Practices for Tracking Impressions

We track impressions for all content created (videos, blog posts, articles, etc). Generally, we measure the total number of impressions after the content has been published for 30 days. In order to stay on track with recording impressions, each team member should create a monthly recurring `Impressions` calendar task on the first day of the month. Each time a piece of content is published, a link should be added to the calendar task. On the first day of the new month, each team member should check impressions for the content and enter the metrics for those items into the [Education Asset Inventory Google Sheet](https://docs.google.com/spreadsheets/d/1k1ci9BPEDDb_CxrR9-uQ_YQD_UXBMDeAU-KTpfQfHD4/edit#gid=0). If it hasn't been 30-days since the content was live, wait to enter the metrics until the next invite. 

#### Summary of Outreach Types

| Asset Type | Metric | Impressions Dashboard | Notes |
| ------ | ------ | -----| ------|
| GitLab Blog | Views | Yes | Blog posts listed in the asset inventory sheet and blog post with `education-program` Post_Type|
| Blog on External Site | Actual Views | Yes | Record after 30 days |
| Case Study | Views | Yes |  |
| YouTube Video GitLab Unfiltered |Views | Yes | |
| YouTube Video GitLab Filtered | Views |Yes | |
| GitLab Landing Page |Views |Yes | |
| Media Articles Impressions | UMV* | Yes | UMVs are separated from actual impressions since they represent potential impressions.  |
| Downloadable Asset | Views | Yes| |
| YouTube (non-GitLab)| Views | Yes | |
| Twitch (WIP) | Unique Viewers | Yes | We don't currently have a Twitch Asset Type but will add one soon. |
| Twitter | Twitter Impressions | No | Twitter impressions are not currently connected to our dashboard. See notes below. |
| LinkedIn | Views on Posts, Articles, Documents | No | LinkedIn views are not current connected to our dashboard. See notes below. |

*UMV is Unique Monthly Visitors and is reported by the media agency.

#### Social Media

We collect Twitter impressions and LinkedIn views as part of our impressions metrics. Social Media impressions originating from Education team member's account are entered in the `Social Media Impressions` tab of the [Education Asset Inventory Google Sheet](https://docs.google.com/spreadsheets/d/1k1ci9BPEDDb_CxrR9-uQ_YQD_UXBMDeAU-KTpfQfHD4/edit#gid=0). This sheet currently serves as our record while we working on adding a social media widget to the dashboard itself.

Social media impressions originating from the official GitLab account at the request of the Education team may be reported at the discretion and availability of the [Social Marketing team](https://about.gitlab.com/handbook/marketing/corporate-marketing/social-marketing/). If you wish to record impressions from an official post, request metrics on the social coverage issue 30 days after the post goes live.

Impressions are recorded at the end of each month and entered into the sheet:

- **Twitter**: Each team member records the total number of Twitter Impressions (original content and retweets) from the Analytics Dashboard on Twitter for each month. If GitLab official tweets are reported upon, include these as well.

- **LinkedIn**: Each team member records the total number of views of posts, articles, and documents for each month.If GitLab official posts are reported upon, include these as well.

### Find us on Slack

GitLab team members can also reach us at any time on the [#education-evangelism](https://gitlab.slack.com/archives/C024ZBWK5KJ) Slack channel where we share updates, ideas, and thoughts with each other and the wider team.

### Requests for support

When we are reviewing opportunities or requests for support, we must be able to answer yes to each of these questions to move forward with the work:
1. Will this work support, grow, and/or engage the GitLab for Education community?
1. Can it grow the skills and abilities of the education evangelist to ultimately make them a better resource to our community?
1. Is there a measurable impact on the GitLab for Education community? This can include influence on a GitLab KPI, progress on an OKR, or completion of responsibilities that are defined in the GitLab handbook.
1. Has an issue been created with the appropriate weights and labels to define the work and assign a DRI?


If the answer to any of the above questions is "no", our team will collaborate with each other and the requestor to do one of the following actions:
1. Make adjustments so we can take on the work.
1. Find another team that is better suited to deliver the work.
1. Come to an agreement that the work should not be done.
