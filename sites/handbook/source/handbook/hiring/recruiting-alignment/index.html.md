---
layout: markdown_page
title: "Talent Acquisition Alignment"
description: "This page is an overview of the search team alignment and the talent acquisition platform directly responsible individual in talent acquisition operations and talent brand."
---

## Search Team Alignment by Department

| Department                    | Recruiter       | Sourcer     |
|--------------------------|-----------------|-----------------|
| Executive          | Rich Kahn   | Chriz Cruz |
| Enterprise Sales, NA | Jonathan Edwards |  TBH |
| Enterprise Sales, EMEA | Debbie Harris |  Kanwal Matharu |
| Enterprise Sales, APAC | Debbie Harris |  Kanwal Matharu |
| Commercial Sales,	AMER | Marcus Carter | TBH  |
| Commercial Sales,	EMEA/APAC | Ben Cowdry | TBH  |
| Channel Sales, Global | Debbie Harris |  Kanwal Matharu |
| Field Operations,	Global | Joanna Muttiah | Loredana Iluca |
| Customer Success, Global | Joanna Muttiah | Loredana Iluca ||
| Marketing, Global | Steph Sarff   | Alina Moise |
| Marketing, SDR Global | Amy Anderson  | Amy Anderson|
| G&A, Finance, People, CEO | Maria Gore | Alina Moise |
| G&A, Accounting, Legal | Rachelle Druffel | Alina Moise |
| Development | Mark Deubel | Zsuzsanna Kovacs |
| Quality | Rupert Douglas   | Zsuzsanna Kovacs |
| UX  | Rupert Douglas   | Zsuzsanna Kovacs  |
| Support | Joanna Michniewicz  |  Joanna Michniewicz |
| Security | Rupert Douglas  |  Zsuzsanna Kovacs |
| Incubation | Rupert Douglas  |  Zsuzsanna Kovacs |
| Infrastructure   | Matt Allen  | Chris Cruz |
| Product Management  | Matt Allen | Chris Cruz |

The Candidate Experience Specialist team operates off of a queue style system utilizing a [GitLab Service Desk](/stages-devops-lifecycle/service-desk/).

## Talent Acquisition Leader Alignment

| Department                    | Leader      | 
|--------------------------|-----------------|
| Talent Acquisition         | Rob Allen |
| Talent Brand and Enablement | Devin Rogozinski |
| Talent Acquisition (Sales) | Jake Foster|
| Talent Acquisition (Marketing) | Kelly Murdock |
| Talent Acquisition (G&A) | Kelly Murdock |
| Talent Acquisition (R&D) | Kelly Murdock|
| Sourcing | Kelly Murdock |
| Candidate Experience | Marissa Feber |

## Talent Acquisition Platform Directly Responsible Individual

| Platform                    | Responsibility        | DRI     |
|--------------------------|-----------------|-----------------|
| Comparably | Admin  | Devin Rogozinski/Marissa Ferber |
| Comparably | Content Management | Devin Rogozinski |
| Comparably | Reporting | Marissa Ferber |
| Glassdoor | Admin  | Devin Rogozinski |
| Glassdoor | Responding to Reviews  | Devin Rogozinski |
| Glassdoor | Content Management | Devin Rogozinski |
| Glassdoor | Reporting | Marissa Ferber |
| LinkedIn | Admin - Recruiter  | Marissa Ferber |
| LinkedIn | Seats | Marissa Ferber |
| LinkedIn | Media - General | Marketing |
| LinkedIn | Media - Talent Acquisition | Devin Rogozinski |
| LinkedIn | Content Management | Marketing |
| LinkedIn | Content Management - Life at GitLab | Devin Rogozinski |
| New Platform(s) | Requests | @domain |
| Recruitment Marketing  | Requests | @domain |
