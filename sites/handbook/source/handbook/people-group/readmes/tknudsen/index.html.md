---
layout: handbook-page-toc
title: "Trevor Knudsen's Readme"
description: "This page is Trevor Kudsen's readme and is intended to be helpful when interacting with him."
---

## Intro

This page shares information specific to Trevor Knudsen, Executive Business Administrator of the People Function. This page is intended to be helpful to those that interact with Trevor.  Please feel free to make any suggestions, or let me know if you feel any information on this page is inaccurate.  

## Related pages

- Executive Business Administrator [Job Description](https://about.gitlab.com/job-families/people-ops/executive-business-administrator/)

## Flaws

- Tendency to overextend self: I love to be helpful, and tend to always say yes to anything I can help with.  Sometimes I need to be reminded that I don't need to take everything on by myself. 
- Iteration: Iteration is a value here at GitLab, and I always strive to iterate.  That being said, it is the value I struggle with most.  I tend to always drive for perfection, and I sometimes hesitate to suggest an idea, hit the merge button, or create an issue unless I feel the idea is perfected. I am always attempting to approve in this aspect, and if you ever see me hesitating, please give me a nudge to action/suggest the idea soonest.
- Feedback: I love getting feedback, though I do not tend to ask for it, or give it.  If you see anything I can improve on, or something that I should continue to do, please give me the feedback.  Also if you are seeking feedback from me, do not hesistate to ask.  I am always working on getting better at providing and asking for feedback.
- Multi-tasking: I am good and taking notes and jotting down things that I need to pick up later, but I do tend to get overwhelmed if too much is thrown at me at once without the ability to prioritize and notate for later.  If I am mid-conversation on one topic, I do get confused if the topic suddently jumps without warning.  
- Timekeeping: For Zoom meetings, this is not too much of an issue, however for in person meetings, I have the mentality that if I'm on time, I'm late. This often leads me to being at meetings, events, or conferences early, and causes me to stress more before events.  I am working on this, and am doing my best to realize that I do not always need to be early.

## Communication

Here are my preferences for ad hoc communications with me:

- Not confidential action items: Slack `@tknudsen` in a public channel (recommended channels include #eba-team, and #people-connect)
- Confidential: Slack @tknudsen in a private channel (create a private channel and invites others as necessary, if needed)
- Confidential to me only: DM Slack `@tknudsen`, or email at `tknudsen@gitlab.com`
- Social/fun: Slack or Zoom please! If you happen to be near where I reside, in person is also great!  Try to avoid email, I like to keep my inbox clean as much as possible.
- Urgent, important, needs conversation: /zoom on Slack via DM
