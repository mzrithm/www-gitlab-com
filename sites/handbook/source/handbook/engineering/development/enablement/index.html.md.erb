---
layout: handbook-page-toc
title: Enablement Sub-department
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Vision

Offer enterprise-grade operational experience of GitLab products from streamlined deployment and maintenance, disaster recovery, secure search and discoverability, to high availability, scalability, and performance.


## Mission

Enablement focuses on improving our capabilities and metrics in the following areas:

 * [Database](/handbook/engineering/development/enablement/database/)
 * [Distribution](/handbook/engineering/development/enablement/distribution/)
 * [Geo](/handbook/engineering/development/enablement/geo/)
 * [Memory](/handbook/engineering/development/enablement/memory/)
 * [Global Search](/handbook/engineering/development/enablement/search/)
 * [Sharding](/handbook/engineering/development/enablement/sharding/)

## Fiscal Year 2022 Direction
 * Grow feature maturity to improve enterprise experience and readiness
 * Build and enhance features and integrations to expand market opportunities
 * Solidify foundation for smooth operations, high availability, and high scalability of SaaS offerings

 For more details, refer to the [Enablement Sub-department Direction](/handbook/engineering/development/enablement/direction.html).

## Sub-department Members

The following people are permanent members of the Enablement Sub-department:

<%
  departments = [
    { name: 'Database', manager: 'craiggomes', regexp: /Database/ },
    { name: 'Sharding', manager: 'craiggomes', regexp: /Sharding|Enablement/ },
    { name: 'Distribution', manager: 'eng-manager-distribution' },
    { name: 'Geo', manager: 'nick-nguyen' },
    { name: 'Memory', manager: 'cliu', regexp: /Memory/ },
    { name: 'Global Search', manager: 'cliu', regexp: /Global Search/ }
 ]
 department_regexp = /(#{Regexp.union(departments.map{ |d| d[:name] })})/
%>

<%=  direct_team(role_regexp: department_regexp, manager_role: 'Director of Engineering, Enablement') %>

## All Team Members

The following people are permanent members of teams that belong to the Enablement Sub-department:

<% departments.each do |dep| %>
### <%= dep[:name] %>
  <%= direct_team(manager_slug: dep[:manager], role_regexp: dep[:regexp]) %>
<% end %>

## Business Continuity - Coverage and Escalation

The following table shows who will provide cover if one or more of the Enablement Engineering management team are unable to work for any reason.

| Team Member     | Issues/MRs Backup        | People Management Backup    | Escalation   |
| -----           | -----                    | -----                 | -----        |
| Chun Du         | Christopher Lefelhocz    | Christopher Lefelhocz | Eric Johnson |
| Craig Gomes     | Kamil Trzcinski (Memory), Andreas Brandl (DB)    | Changzheng Liu        | Chun Du      |
| Changzheng Liu  | Dylan Griffith           | Craig Gomes           | Chun Du      |
| Steven Wilson   | DJ Mountney              | Nick Nguyen           | Chun Du      |
| Nick Nguyen     | Douglas Alexandre        | Steven Wilson         | Chun Du      |

If an Engineer is unavailable the person providing `Issues/MRs` coverage will reassign open issues and merge requests to another engineer, preferably in the same [group](#all-team-members).

Some people management functions may require escalation or delegation, such as BambooHR and Expensify.

This can be used as the basis for a BCP [Communication Plan and Role Assignments](/handbook/business-ops/gitlab-business-continuity-plan/#communication-plan-and-role-assignments),
as well as a general guide to Enablement Engineering continuity in the event of one or more team members being unavailable for any reason.

## Stable Counterparts

The following members of other functional teams are our stable counterparts:

<%= stable_counterparts(role_regexp: /[,&] Enablement|Enablement:/, direct_manager_role: 'Director of Engineering, Enablement') %>

## How We Work

### Professional Development
1. Conference, meetup, and training: To request budget for these career development events, please refer to [budget application for professional development](./processes/budget_approval.html).
2. Career development plan: It is recommended that the manager and the individual collaboratively develop a career development plan and discuss progress via career talks regularly. Here is an [Enablement sub-department career development plan template](https://docs.google.com/document/d/1LEyTuJIcdGeMuiqBojiLTHHdc1oQonSlOhPqUTTnSkQ/edit#heading=h.sw0a6mizbgaq).
3. Promotion: An individual is ready for promotion when both the individual and the manager reach consensus that the individual is performing job competencies at the next level of career ladder according to the [Engineering Career Framework](/handbook/engineering/career-development/matrix/engineering/).
   * Managers, when proposing a promotion for your team member use the [PeopleOps promotion document template](https://docs.google.com/document/d/1SavyZeQRXY4fOzv0Y7xMv4TiuWQIf51YtMptr_ZWFQs/edit#).

### Combined Team
Based on the scale of frontend engineering in Enablement sub-department, the [Combined Team](/handbook/engineering/#combined-teams) model is being experimented where both Frontend and Backend Engineers report to a single Engineering Manager. It's believed that the combined team model enables higher efficiency for the situation of Enablement and the experiment is subject to continuous review. The team will return to functional model by establishing a dedicated frontend engineering team when the frontend engineering team reaches the manager to individual contributor ratio guideline.

### Dogfood Roadmap in Planning
To enhance the overview of team direction, all Enablement development teams use Epics to organize projects and dogfood the [Roadmap](https://docs.gitlab.com/ee/user/group/roadmap/index.html) feature to communicate short- and mid-term product development roadmap, although each team customizes their usage such as applying specific labels. Examples:
* [Advanced Search roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=group%3A%3Aglobal%20search&label_name%5B%5D=Roadmap)
* [Geo roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Ageo)
* [Memory roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Amemory&label_name[]=Roadmap)
* [Database roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Roadmap&label_name[]=group%3A%3Adatabase)

### Long Standing Merge Requests
* Engineering managers and team members will periodically review the [long standing MRs](https://app.periscopedata.com/app/gitlab/576886/New-Enablement-Section-Overview-Dashboard?widget=7530886&udv=0) and decide appropriate actions for MRs lasting more than 30 days.
* From time to time, MRs are created without intention to merge, such as research spikes, novel ideas, etc. In this case, always prefix the MR subject line with `WIP`. These MRs exempt from the above review.

| WIP in Subject? | > 30 days? | Action Required? |
| ------ | ------ | ------ |
| Yes | Yes | No |
| Yes | No | No |
| No | Yes | Yes |
| No | No | No |

### Increasing Efficiency through Documenting Decisions
Documenting development decisions is another way to increase efficiency.  These decisions can be either in an issue explicitly stating that we will not work on this issue, the product category page for your group or a more formal decision log in your group's section of the handbook.  Whatever your chosen desitination, each group should try to maintain a [single source of truth](https://about.gitlab.com/handbook/values/#single-source-of-truth) for the decisions.  A recent example (without mentioning specific product name) had a development team researching an open source product to accelerate development time only to find out later that this research had been previously completed and the product was eliminated from consideration.  If this decision had been discoverable via documentation or issue it would have saved precious development time.

We have started creating decision logs to benefit our internal development team as well as our greater GitLab community.  It is up to each group to determine the best location for decision logs to be discoverable.  For example, the Database team has a decision log for [Sharding GitLab with CitusDB](https://about.gitlab.com/handbook/engineering/development/enablement/database/doc/citus.html) in the Enablement/Database section of the handbook and a decision log for the [Sharding Working Group](https://about.gitlab.com/company/team/structure/working-groups/sharding/#outcome---closed) in the working group section of the handbook.

For issues, a clear decision is when an issue is successfully closed.  However, if an issue is closed because we "won't do it" it may not be immediately clear.  We are adopting the `~won't do` label for those issues.  Often the pattern is to just stash these issues in the `~backlog`.  This can be misleading to those watching the issue and frustrating to the original author, especially if they are a community contributor.  When we apply a `won't do` label to an issue, we are making a clear decision.  If there is no pushback on the `won't do` label then we made the right decision.  If there is pushback and we need to reprioritize the issue, then that is a good outcome as well.

## Software Subscriptions
The Enablement teams leverage the following software or SaaS services to deliver our products. For general information of procurement, checkout the [procurement team page](/handbook/finance/procurement/). For procurement questions, bring to [#procurement](https://gitlab.slack.com/archives/CPTMP6ZCK). To make new Purchase Order (PO), create a new [procurement issue](https://gitlab.com/gitlab-com/Finance-Division/procurement-team/procurement/-/issues/new?issuable_template=software_vendor_contract_request).

| Software | Vendor Link | Term    | Renewal Date | Team Impacted | Comments |
| -----    | -----       | -----   | -----      | -----         | -----    |
| packagecloud.io | [https://packagecloud.io/](https://packagecloud.io/) | Annual | March 30th | Distribution | Existing vendor, [last renewal issue](https://gitlab.com/gitlab-com/finance/-/issues/2304)  |
| dependencies.io | [https://www.dropseed.io/](https://www.dropseed.io/ ) | Annual | November 1st | Distribution | Existing vendor, [last renewal issue](https://gitlab.com/gitlab-org/distribution/team-tasks/-/issues/514) |
| scaleway        | [https://www.scaleway.com/en/](https://www.scaleway.com/en/) | Monthly | End of Month | Distribution | Existing vendor |
| postgres.ai     | [https://postgres.ai/](https://postgres.ai/)     | Trial  | TBD          | Database     | Existing vendor, [request issue](https://gitlab.com/gitlab-org/database-team/team-tasks/-/issues/47) |


## Common Links

* Issue Boards
  * [Database Milestone Board](https://gitlab.com/groups/gitlab-org/-/boards/1318796?label_name[]=group%3A%3Adatabase)
  * [Distribution - Deliverable Board](https://gitlab.com/groups/gitlab-org/-/boards/1282058?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Deliverable&label_name[]=group%3A%3Adistribution&milestone_title=%23started)
  * [Geo Kanban Board](https://gitlab.com/groups/gitlab-org/-/boards/1181257?label_name[]=geo%3A%3Aactive)
  * [Memory Milestone Board](https://gitlab.com/groups/gitlab-org/-/boards/1143987?label_name[]=group%3A%3Amemory)
  * [Global Search Milestone Board](https://gitlab.com/groups/gitlab-org/-/boards/1339901?label_name[]=group%3A%3Aglobal%20search)
* Slack Channels
  * Enablement Sub-department [#s_enablement](https://app.slack.com/client/T02592416/CMPK8QBB9)
  * Database team [#g_database](https://gitlab.slack.com/app_redirect?channel=g_database)
  * Distribution team [#g_distribution](https://gitlab.slack.com/messages/C1FCTU4BE)
  * Geo team [#g_geo](https://gitlab.slack.com/messages/C32LCGC1H)
  * Memory team [#g_memory](https://gitlab.slack.com/messages/CGN8BUCKC)
  * Global Search team [#g_global_search](https://gitlab.slack.com/app_redirect?channel=g_global_search)
