---
layout: handbook-page-toc
title: Fulfillment Sub-department
description: "The Fulfillment Sub-department is composed of development teams, Purchase, License, and Utilization, working on the infrastructure between the systems which affect the user purchasing process that support the GitLab DevOps Platform."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Vision

The Fulfillment Engineering team has grown substantially with an influx of new staff through hiring and internal realignments over the previous year.  Our success in FY22 will be determined by how well we can up-skill and motivate these new team members along with the relationships and trust we build with our stable counterparts in Product, UX and Quality while we work to deliver our team's roadmap.

In order to support GitLab's continued business growth, the applications we are responsible for need to be able to scale while reducing our customer’s needs for manual support intervention.  To achieve this, our Engineering efforts and product roadmap are concentrated around ensuring our Fulfillment process:

* Is a delightful, performant and error free purchasing and licensing experience for our customers.
* Provides our customers the tools they need to manage their accounts and licenses.
* Improves and automates more of the complexity of our sales process in order to reduce processing and intervention by our sales teams.

We measure our success by our [performance indicators](https://about.gitlab.com/handbook/engineering/development/performance-indicators/fulfillment/) and our team members engagement and wellbeing.  We support our individual team members by having regular career conversations, [gathering actionable feedback](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/10558) on ways we can improve the way we work, and reinforcing our [Diversity, Inclusion and Belonging](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/10627) value through ongoing training.  We aim to run quarterly team days to help build relationships and foster collaboration.

## Mission

Fulfillment focuses on improving our capabilities and metrics in the following areas:

* Fulfillment: [Direction](https://about.gitlab.com/direction/fulfillment), [Group Responsibilities](https://about.gitlab.com/direction/fulfillment/#group-responsibilities), [Fulfillment Development Team](/handbook/engineering/development/fulfillment/)
* Purchase: [Direction](https://about.gitlab.com/direction/fulfillment/purchase), [Group Responsibilities](https://about.gitlab.com/direction/fulfillment/#purchase), [Backend Team](/handbook/engineering/development/fulfillment/be-purchase/), [Frontend Team](/handbook/engineering/development/fulfillment/fe-purchase-license-utilization/)
* License: [Direction](https://about.gitlab.com/direction/fulfillment/license), [Group Responsibilities](https://about.gitlab.com/direction/fulfillment/#license), [Backend Team](/handbook/engineering/development/fulfillment/be-license/), [Frontend Team](/handbook/engineering/development/fulfillment/fe-purchase-license-utilization/)
* Utilization: [Direction](https://about.gitlab.com/direction/fulfillment/utilization), [Group Responsibilities](https://about.gitlab.com/direction/fulfillment/#utilization), [Backend Team](/handbook/engineering/development/fulfillment/be-utilization/), [Frontend Team](/handbook/engineering/development/fulfillment/fe-purchase-license-utilization/)

## Team members

### Fulfillment People Leaders

<% fulfillment_em = %w[jeromezng chris_baus jameslopez csouthard rhardarson] %>
<% fulfillment_em_members = Gitlab::Homepage::Team::Member.all!.find_all { |person| fulfillment_em.include?(person.anchor) } %>
<%= partial('includes/team_member_table', locals: { team_members: fulfillment_em_members }) %>

### Fulfillment:Purchase BE Team

<%= department_team(base_department: "Fulfillment:Purchase BE Team") %>

### Fulfillment:Purchase FE Team

<%= department_team(base_department: "Fulfillment:Purchase FE Team") %>

### Fulfillment:License BE Team

<%= department_team(base_department: "Fulfillment:License BE Team") %>

### Fulfillment:License FE Team

<%= department_team(base_department: "Fulfillment:License FE Team") %>

### Fulfillment:Utilization BE Team

<%= department_team(base_department: "Fulfillment:Utilization BE Team") %>

### Fulfillment:Utilization FE Team

<%= department_team(base_department: "Fulfillment:Utilization FE Team") %>

## Stable counterparts

### Fulfillment Product Management

<%= department_team(base_department: "Fulfillment PM Team") %>

### Fulfillment Quality Engineering

<%= department_team(base_department: "Fulfillment & Growth QE Team") %>

### Fulfillment UX

<%= department_team(base_department: "Fulfillment UX Team") %>

**Technical Writing**

<% fulfillment_tw = %w[sselhorn] %>
<% fulfillment_tw_members = Gitlab::Homepage::Team::Member.all!.find_all { |person| fulfillment_tw.include?(person.anchor) } %>
<%= partial('includes/team_member_table', locals: { team_members: fulfillment_tw_members }) %>

### Fulfillment Security

<% fulfillment_sec = %w[ankelly] %>
<% fulfillment_sec_members = Gitlab::Homepage::Team::Member.all!.find_all { |person| fulfillment_sec.include?(person.anchor) } %>
<%= partial('includes/team_member_table', locals: { team_members: fulfillment_sec_members }) %>

## How we work

* In accordance with our [GitLab values](/handbook/values/)
* Transparently: nearly everything is public, we record/livestream meetings whenever possible
* We get a chance to work on the things we want to work on
* Everyone can contribute; no silos

### Planning

We plan in monthly cycles in accordance with our [Product Development Timeline](/handbook/engineering/workflow/#product-development-timeline).
Release scope for an upcoming release should be finalized by the `1st`.

On or around the `26th`: Product meets with Engineering Managers for a preliminary issue review. Issues are tagged with a milestone and are estimated initially.

#### Purchase

* [13.11 Board](https://gitlab.com/groups/gitlab-org/-/boards/2471198?milestone_title=13.11&&label_name%5B%5D=group%3A%3Apurchase)
* [13.11 Frontend Board](https://gitlab.com/groups/gitlab-org/-/boards/2341677?&label_name%5B%5D=devops%3A%3Afulfillment&label_name%5B%5D=frontend)
* [13.11 Planning Issue](https://gitlab.com/gitlab-org/fulfillment-meta/-/issues/168)

#### License

* [13.11 Board](https://gitlab.com/groups/gitlab-org/-/boards/2474045?milestone_title=13.11&&label_name%5B%5D=group%3A%3Alicense)
* [13.11 Planning Issue](https://gitlab.com/gitlab-org/fulfillment-meta/-/issues/183)

#### Utilization

* [13.11 Board](https://gitlab.com/groups/gitlab-org/-/boards/2473827?milestone_title=13.11&&label_name%5B%5D=group%3A%3Autilization)
* [13.11 Planning Issue](https://gitlab.com/gitlab-org/fulfillment-meta/-/issues/187)


### Estimation

Before work can begin on an issue, we should estimate it first after a preliminary investigation. This is normally done in the monthly planning meeting.

| Weight | Description (Engineering) |
| ------ | ------ |
| 1 | The simplest possible change. We are confident there will be no side effects. |
| 2 | A simple change (minimal code changes), where we understand all of the requirements. |
| 3 | A simple change, but the code footprint is bigger (e.g. lots of different files, or tests effected). The requirements are clear. |
| 5 | A more complex change that will impact multiple areas of the codebase, there may also be some refactoring involved. Requirements are understood but you feel there are likely to be some gaps along the way. |
| 8 | A complex change, that will involve much of the codebase or will require lots of input from others to determine the requirements.
| 13| A significant change that may have dependencies (other teams or third-parties) and we likely still don't understand all of the requirements. It's unlikely we would commit to this in a milestone, and the preference would be to further clarify requirements and/or break in to smaller Issues.

In planning and estimation, we value [velocity over predictability](/handbook/engineering/#velocity-over-predictability). The main goal of our planning and estimation is to focus on the [MVC](/handbook/values/#minimal-viable-change-mvc), uncover blind spots, and help us achieve a baseline level of predictability without over optimizing. We aim for 70% predictability instead of 90%. We believe that optimizing for velocity (MR throughput) enables our Fulfillment teams to achieve a [weekly experimentation cadence](/handbook/product/growth/#weekly-growth-meeting).

- If an issue has many unknowns where it's unclear if it's a 1 or a 5, we will be cautious and estimate high (5).
- If an issue has many unknowns, we can break it into two issues. The first issue is for research, also referred to as a [Spike](https://en.wikipedia.org/wiki/Spike_(software_development)), where we de-risk the unknowns and explore potential solutions. The second issue is for the implementation.
- If an initial estimate is incorrect and needs to be adjusted, we revise the estimate immediately and inform the Product Manager. The Product Manager and team will decide if a milestone commitment needs to be adjusted.

#### Estimation Template

The following is a guiding mental framework for engineers to consider when contributing to estimates on issues.

```
### Refinement / Weighting

**Ready for Development**: Yes/No

<!--
Yes/No

Is this issue sufficiently small enough, or could it be broken into smaller issues? If so, recommend how the issue could be broken up.

Is the issue clear and easy to understand?
-->

**Weight**: X

**Reasoning**:

<!--
Add some initial thoughts on how you might breakdown this issue. A bulleted list is fine.

This will likely require the code changes similar to the following:

- replace the hexdriver with a sonic screwdriver
- rewrite backups to magnetic tape
- send up semaphore flags to warn others

Links to previous example. Discussions on prior art. Notice examples of the simplicity/complexity in the proposed designs.
-->


**MR Count**: Y
<!--
- 1 MR to update the driver worker
- 1 MR to update docs regarding mag tape backups

Let me draw your attention to potential caveats.
-->

**Testing considerations**:
<!--
- ensure that rotation speed of sonic screwdriver doesn't exceed rotational limits
-->
```

### Picking something to work on

Engineers can find and open [the milestone board for Fulfillment](https://gitlab.com/groups/gitlab-org/-/boards/)
and begin working first on those with the `deliverable` label.

It's possible for engineers to pick any of the remaining issues for the milestone once the deliverables are done. If the engineer has no preference, they can choose the next available issue from the top.

The following table will be used as a guideline for scheduling work within the milestone:

| Type        | % of Milestone | Description                                                                                |
|-------------|----------------|-------------|------------------------------------------------------------------------------|
| Deliverable | 40%            | business priorities (compliance, IACV, efficiency initiatives)                             |
| Bug         | 16%            | non-critical bug fixes                                                                     |
| Tech debt   | 10%            |                                                                                            |
| Features    | 14%            | non-critical                                                                               |
| Other       | 20%            | engineer picks, critical security/data/availability/regression, urgent business priorities |

#### Organizing the work

We generally follow the [Product Development Flow](/handbook/product-development-flow/#workflow-summary):
1. `workflow::problem validation` - needs clarity on the problem to solve
1. `workflow::design` - needs a clear proposal (and mockups for any visual aspects)
1. `workflow::solution validation` - designs need to be evaluated by customers, and/or other GitLab team members for usability and feasibility
1. `workflow::planning breakdown` - needs a Weight estimate
1. `workflow::scheduling` - needs a milestone assignment
1. `workflow::ready for development`
1. `workflow::in dev`
1. `workflow::in review`
1. `workflow::verification` - code is in production and pending verification by the DRI engineer

Generally speaking, issues are in one of two states:
* Discovery/refinement: we're still answering questions that prevent us from starting development,
* Implementation: an issue is waiting for an engineer to work on it, or is actively being built.

Basecamp thinks about these stages in relation to the [climb and descent of a hill](https://www.feltpresence.com/hills.html).

While individual groups are free to use as many stages in the [Product Development Flow](/handbook/product-development-flow/#workflow-summary) workflow as they find useful, we should be somewhat prescriptive on how issues transition from discovery/refinement to implementation.

### Weekly async issue updates

Every Friday, each engineer is expected to provide a quick async issue update by commenting on their assigned issues using the following template:

```
<!---
Please be sure to update the workflow labels of your issue to one of the following (that best describes the status)"
- ~"workflow::In dev"
- ~"workflow::In review"
- ~"workflow::verification"
- ~"workflow::blocked"
-->
### Async issue update
1. Please provide a quick summary of the current status (one sentence).
1. When do you predict this feature to be ready for maintainer review?
1. Are there any opportunities to further break the issue or merge request into smaller pieces (if applicable)?
1. Were expectations met from a previous update? If not, please explain why.
```

We do this to encourage our team to be more async in collaboration and to allow the community and other team members to know the progress of issues that we are actively working on.

### Quality

GitLab's Quality is everyone's responsibility. The [SETs](/handbook/engineering/quality/#stable-counterparts) embedded within Fulfillment section follow the [Quality Engineering Departments's principles](https://about.gitlab.com/handbook/engineering/quality/#our-principles) primarily to ensure that everyone is aware of product quality. 

#### Quad-planning workflow

The SETs helps facilitate the quad-planning process. This is the participation of Product Management, Development, UX, and the Quality team which aims to bring test planning as a topic before the development of any feature.

We follow the Quad Planning process defined [here](https://about.gitlab.com/handbook/engineering/quality/quad-planning/#process). 

[Quad Planning Dashboard](https://app.periscopedata.com/app/gitlab/634305/Quad-Planning-Dashboard?widget=8844548&udv=1040834) showcases the total Planned issues for Quad Planning vs the actual ones for each milestone.

#### Tests

The CustomersDot has different types of tests running:

1. Linting and [rubocop](https://github.com/rubocop-hq/rubocop) jobs
1. Unit tests (specs, these could be of many types, such as controller specs)
1. Integration tests (specs, mocking external calls)
1. Frontend tests
1. E2E integration tests [via Watir](https://github.com/watir/watir)

We also have a flag `VCR` that mocks external calls to Zuora by default. We have a [daily pipeline](https://gitlab.com/gitlab-org/customers-gitlab-com/pipeline_schedules) that runs at 9AM UTC with the flag set so the API calls hit the Zuora sandbox and we are notified of any failure (due to potential API changes).

Any test failure is notified to #s_fulfillment_status including a link to the pipeline. Pipeline failures will prevent deployments to staging and production.

### Deployment

### Release of Revenue Impacting Changes

Fulfillment is unique in Engineering because our changes can directly impact revenue. Changes which impact revenue or are otherwise high risk may require broader scrutiny including input from teams outside of Fulfillment. PM is the DRI on these decisions, taking input from stakeholders including Sales, Enterprise Applications, Marketing, Finance, and Customer Support.

We aspire to be as iterative as possible including even when a change potentially poses high risk.

Below are examples of changes that can be high risk because they can require coordination across many teams:
* Pricing changes
* Billing changes
* Launching a new paid feature
* Deprecating an existing paid feature
* Changes to terms of service and related contract changes

Below are changes that can often be low risk:
* Backend enhancements that are not yet used by the frontend
* Frontend and backend Enhancements that are behind a feature flag
* Frontend enhancements that are not behind a feature flag, but are labeled as "beta" or "experimental"

The following processes should be followed for high-risk changes:

* Use confidential issues for these changes
* Release changes under a feature flag so this can be enabled on the release date
* Create a rollout issue for the release date and make sure all parties are informed about the process. [Example](https://gitlab.com/gitlab-org/gitlab/-/issues/299068)
* Consider enabling this to a subset of customers so changes can be tested iteratively
* Use the regular MR process even if the issue is confidential until confidential MRs are improved ([1](https://gitlab.com/groups/gitlab-org/-/epics/1175), [2](https://gitlab.com/groups/gitlab-org/-/epics/264))

#### CustomersDot

We use CD (Continuous Deployment) for [CustomersDot](https://gitlab.com/gitlab-org/customers-gitlab-com/) and a MR goes through the following stages once it gets merged into the `staging` branch:

```mermaid
graph TD;
    A(Merged) --> |Green tests| B(Staging);
    B --> C[E2E Test on Staging in 40 minutes];
    C --> D[Verification]
    D --> E(Auto deploy to production in 2 hours);
```

If something goes wrong at the `Verification` stage, we could create an issue with the label `production::blocker`, which will prevent deployment to production. The issue cannot be confidential.

For MRs with significant changes, we should consider using [feature flags](https://gitlab.com/gitlab-org/customers-gitlab-com/#feature-flags-unleash) or create an issue with the `production::blocker` label to pause deployment and a allow for longer testing.


#### LicenseDot

We use an [automatic deployment to staging, but manual deployment to production](https://docs.gitlab.com/ee/topics/autodevops/#deployment-strategy) for LicenseDot.

Maintainers of the application need to trigger a manual action on the `master` branch in order to deploy to production.

The app lives at https://license.gitlab.com

The `staging` environment can be found [here](https://gitlab-org-license-gitlab-com-staging.license-stg.gitlab.org)

#### Escalation Process

In most cases an MR should follow the standard process of review, maintainer review, merge, and deployment as outlined above. When production is broken:

1. First determine whether the [Rapid Engineering Response](/handbook/engineering/workflow/#rapid-engineering-response) process can be followed. This will depend on availability and how critical the situation is.
1. The three hour wait between Staging and Production auto deploy can be bypassed with a manual deployment by a Maintainer.
1. When there are no project maintainers available ([CustomersDot](/handbook/engineering/projects/#customers-app), [LicenseDot](/handbook/engineering/projects/#license-app)) an additional GitLab Team Member with Maintainer access can be asked to assist.

In these cases please ensure:

1. There is an issue describing the reasons for the escalation, as per the [Rapid Engineering Response](/handbook/engineering/workflow/#rapid-engineering-response). Consider '@'mentioning the relevant [Growth/Fulfillment teams](https://gitlab.com/gitlab-org/growth).
1. The change is announced on the [#s_fulfillment](https://gitlab.slack.com/archives/CMJ8JR0RH) channel.

#### Feature freeze

The feature freeze for Fulfillment occurs at the same time as the rest of the company, normally around the 18th.

| App | Feature freeze (*) | Milestone ends |
| ---      |  ------  |----------|
| GitLab.com   | ~18th-22nd   | Same as the freeze |
| Customers/License   | ~18th-22nd   | Same as the freeze |

(*) feature freeze may vary according to the [auto-deploy document](https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/deploy/auto-deploy.md).

Any issues not merged on the current milestone post feature freeze, will need to be moved to the next one (priority may also change for those).

### Measuring Engineering Performance

See the [Fulfillment Section Performance Indicators](/handbook/engineering/development/performance-indicators/fulfillment/) as well as the
[Centralized Engineering Dashboards](/handbook/engineering/metrics/).

### CustomersDot Engineer:Maintainer ratio

<embed width="<%= 99 %>%" height="350" src="<%= signed_periscope_url(dashboard: 825250, chart: 11035807, embed: 'v2') %>">

### Meetings (Sync)

We hold optional synchronous social meetings weekly where we chat about anything outside work.
These meetings happen each Wednesday alternating between 10:00am UTC (EMEA) and 4:00pm (AMER).
Check the [Fulfillment Google calendar](https://calendar.google.com/calendar/embed?src=gitlab.com_7199q584haas4tgeuk9qnd48nc%40group.calendar.google.com&ctz=America%2FBogota) for more information.

### Retrospectives

After the `8th`, the Fulfillment team conducts an [asynchronous retrospective](/handbook/engineering/management/team-retrospectives/). You can find current and past retrospectives for Fulfillment in [https://gitlab.com/gl-retrospectives/fulfillment/issues/](https://gitlab.com/gl-retrospectives/fulfillment/issues/).

### Onboarding

To be added


## Iteration Retrospectives

There are several resources at GitLab for improving our iteration capability:

- [engineering iteration documentation](https://about.gitlab.com/handbook/engineering/workflow/iteration/)
- [product iteration documentation](https://about.gitlab.com/handbook/product/product-principles/#iteration)
- [self-guided iteration training issue](https://gitlab.com/gitlab-com/Product/-/issues/new?issuable_template=iteration-training)
- [iteration office hours](https://about.gitlab.com/handbook/ceo/#iteration-office-hours)
- [milestone retrospectives](https://about.gitlab.com/handbook/engineering/development/fulfillment/#retrospectives)
- [iteration retrospectives](#iteration-retrospectives)

Agile software development practices uses the phrase `Iteration Retrospective` to describe what we perform during our [Milestone Retrospective](https://about.gitlab.com/handbook/engineering/development/fulfillment/#retrospectives). While the Milestone Retrospective is essential in understanding how we can improve, it does have a broad focus across multiple teams. It's a safe space where everyone can contribute through reflection on what we can continue, what we can correct, and what we can improve.

An Iteration Retrospective, in the GitLab values sense that we're using here, is more focused on a specific example (or few examples) of a **successful or unsuccessful** iteration attempt. That is, an Issue, Epic, or merge request that we can examine deeply to understand what we can do differently in the future to be more iterative and efficient. These retrospectives will provide a growth opportunity to work on developing an iterative mindset - build it, then build it better.

Each group should perform their own Iteration Retrospective, but share as transparently as possible to help fellow teammates and other teams learn from your investigation.

### Frequency

Iteration Retrospectives should be conducted quarterly.

### Preparation

Engineering Manager(s) or Other team members should review current or previous milestones to find a good, challenging candidate issue for the Iteration Retrospective. Choose one (or two if related) from the suggestions, then create a retrospective issue.

Review [these rules](https://about.gitlab.com/handbook/engineering/management/group-retrospectives/) for an effective retrospective.

### Participation

Everyone can and should contribute asynchronously to the discussion. Present your ideas on alternative methods or mechanisms to break down the problem stated in chosen issue for review. If the problem breakdown was fine, suggestions about process or other refinements are also welcome.

Don't belabor the point, participation should be timeboxed to 1 week. A 1 week due date quick action is available in the template below.

Consider handbook updates, process changes, and bubbling up information from your conclusions after this retrospective.

### Template (optional)

```
Iteration is one of six GitLab Values, but also really difficult. By focusing how we have iterated well in the past and how we have not will help us iterate faster. Please contribute the following [Iteration Retrospective](#link-to-handbook-page).

## Summary

<!--
Include a brief summary of the issue. Consider including key outcomes, significant changes, and impact.
-->

`Summary of the Issue`

## Details

<!--
Complete the following details when creating the issue.
-->

- Did the team meet the iteration goals? Why or why not?
- How many MRs were created to complete the Iteration?
- What were the Days to Merge for the MRs related to the this issue?
- Are there successes or opportunities for improvement with respect to collaboration with peers or stable counterparts?
- Could the original issue have been broken down into smaller components?
- Could the MR(s) have been broken down into smaller components?

## Tasks

**Read**

- [Iteration Value](https://about.gitlab.com/handbook/values/#iteration)
- [Engineering Iteration](https://about.gitlab.com/handbook/engineering/workflow/iteration/)
- [Why iteration helps increase the merge request rate](https://about.gitlab.com/blog/2020/05/06/observations-on-how-to-iterate-faster/)

**Watch**

- [Interview about iteration in engineering with Christopher and Sid](https://www.youtube.com/watch?v=tPTweQlBS54)
- [Iteration Office Hours with CEO](https://www.youtube.com/watch?v=liI2RKqh-KA)


**Contribute**

In the threads below consider the following:

- Why was it (un)successful? If successful, how did it meet the definition of an [MVC](https://about.gitlab.com/handbook/product/product-principles/#the-minimal-viable-change-mvc)?
- How did the example not meet the definition of an [MVC](https://about.gitlab.com/handbook/product/product-principles/#the-minimal-viable-change-mvc)? In what ways could you have iterated differently? List specific examples.
- Identify areas of improvement for the team that can be incorporated into our processes and workflows.

Follow [these rules](https://about.gitlab.com/handbook/engineering/management/group-retrospectives/) for an effective retrospective.

Check your name off the completed list when all tasks are complete.

**Completed**

<!--
When creating this issue replace the person placeholders with teamembers and select stable counterparts as you see fit.
-->

- [ ] Person
- [ ] Person
- [ ] Person
- [ ] Person

**Next Steps**

- [ ] Engineering Manager to update your team's handbook page with ideas and improvements you've incorporated to improve iteration.

## Thread Prompts

<!--
Use these prompts or similar to start conversation threads.
-->

1. What aspects of the Issue/Epic and resulting MR(s) were very good/bad examples of Iteration and why?
1. Does anyone have alternative ideas for how this work could have been broken down?
1. Is there anything that we can change in our processes or the way we work that could improve our iteration as a team?


/due in 1 week
/label ~"Iteration Retrospective"
```

## OKRs

* [FY22 Q2 Quad OKR's](https://gitlab.com/gitlab-org/fulfillment-meta/-/issues/169) (In Progress)
* [FY22 Q1 Engineering OKR's](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/10680)

## Performance indicators

To be added

## Knowledge sharing

- Generating test licenses, adding them to GitLab, and then finding and removing them from the GitLab database
  - [Document with notes](https://docs.google.com/document/d/1AvGz5MaU0nYt-q_dC8IjXxnWFHyAQhPb8QgPkzS3eZI/edit)
  - [Recording](https://drive.google.com/file/d/1cIkm2anBaN1ToPBjNYujJXGC4X4avnAj/view?usp=sharing)
- New Integration with Sentry
  - [Document with notes](https://docs.google.com/document/d/1rC1cOGCnVMBSHpc1BAgz10p62M1lACAB6Nk1bJnRcHc/edit#heading=h.y7egyzvjh7bg)
  - [Recording](https://drive.google.com/file/d/1mN4Q776SxhNON8UIm55YrXXqPQl2VaDR/view?usp=sharing)
- Intro to FE GraphQL at GitLab with Vue Apollo
  - [Slides](https://docs.google.com/presentation/d/1lOTisAIWn2u1pcZUgkXZkhCOr3kAVnoyNUyZ17T5GFI/edit#slide=id.g29a70c6c35_0_68)
  - [Recording](https://drive.google.com/file/d/1jgyb02XE3sznojdx3RjRFn8QAfF90YIf/view?usp=sharing)
- [CustomersDot Resource Video Library](https://gitlab.com/gitlab-org/customers-gitlab-com/-/blob/staging/doc/resource_videos.md)
- [Zuora University Select Topics](https://university.zuora.com/series/courses-by-topic#filter-by-9-keys_integrations)
- [Confidential Issue Git Pre-push Hook](https://gitlab.com/gitlab-org/gitlab/-/issues/332471) to prevent any accidental push to any non-Gitlab Security remote whenever a developer works on a _confidential issue_. Reference: [How to customize Git Hooks](https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks).

## Common links

* [Product Vision](https://about.gitlab.com/direction/fulfillment/)
* [Fulfillment section](/handbook/product/product-categories/#fulfillment-section)
* [InfraDev Team initiative](/handbook/engineering/development/fulfillment/infradev/)
* [Fulfillment Software Architecture](/handbook/engineering/development/fulfillment/architecture)
* [All open Fulfillment epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Afulfillment)
* [Issue Tracker](https://gitlab.com/gitlab-org/fulfillment-meta/issues)
* [Slack channel #s_fulfillment](https://gitlab.slack.com/app_redirect?channel=s_fulfillment)
* [Daily standup channel #s_fulfillment_daily](https://gitlab.slack.com/app_redirect?channel=s_fulfillment_daily)
* [Team calendar](https://calendar.google.com/calendar/embed?src=gitlab.com_7199q584haas4tgeuk9qnd48nc%40group.calendar.google.com)
* [Fulfillment technical debt status](https://app.periscopedata.com/app/gitlab/618368/Growth-technical-debt-status)

