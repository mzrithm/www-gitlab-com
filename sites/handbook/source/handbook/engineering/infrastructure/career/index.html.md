---
layout: handbook-page-toc
title: "Career Development in the Infrastructure Department"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Tools

There are a number of tools we use to plot and manage career development:

* Role descriptions, which outline role responsibilities, requirements and nice-tohaves for each level in the role.
  * [DBRE](/job-families/engineering/infrastructure/database-reliability-engineer/)
  * [SRE](/job-families/engineering/infrastructure/site-reliability-engineer/)
  * [Backend Developer, Delivery](/job-families/engineering/backend-engineer/)
  * [Backend Developer, Infrastructure](/job-families/engineering/backend-engineer/)
* [Compa Ratios](/handbook/total-rewards/compensation/compensation-calculator/#compa-ratio), which reflects demonstrated ability to drive projects and deliverables from the position description in a quantifiable manner.
* Quarterly checkpoints
* 1:1s
* [360 Feedback](/handbook/people-group/360-feedback/)

## Workflow

* **Quarterly Checkpoints**
  * On the first 1:1 meeting of the quarter, discuss the EFW and select two or three role responsibility areas to focus on during the quarter. Make a note of them in the 1:1 document and add them to the standing agenda. For each area, outline specific goals and targets to achieve, as well as ideas to work on them.
  * On the last 1:1 meeting of the quarter, discuss and record a summary of the progress made for the quarter and update the EFW accordingly.
* **1:1s**: Mentor on the focus areas in weekly 1:1s at a frequency that feels right for the tasks. There is no hard rule to discuss the career development items every week, but try to do so regularly and avoid a wide-open, ad-hoc conversation.

There is no rule that guides the amount of progress that should be made in a given time-period in a strict fashion. We should however strive to set targets to progress to the [next level](/handbook/total-rewards/compensation/compensation-calculator/#introduction) on at least a quarterly basis.

Actions to make changes to a GitLab team-member's level can be taken during the [360 Feedback](/handbook/people-group/360-feedback/), and the data collected throughout this workflow should be useful at that time.

### Managers

You must prepare to provide mentoring and guidance on focus areas for the quarter, especially in terms of how to improve and evolve in said areas. Do not hesitate to ask for help if you feel you need guidance to assess strategies for mentorship, and feel free to reach out to the other GitLab team-members who can provide said guidance. Be clear and concise in setting expectations, trying to align them with work that will take place during the quarter.

### Individual Contributors

Think about the areas that are important to you and commit to working on them over the quarter. Keep an open mind to receive feedback and guidance on these areas, and be an active participant in the process.

## Guidelines and Considerations

Career development requires that all parties involved be committed to the process and are active participants.

One implicit area to focus on explicitly is **communication**. Regardless of your role, whether technical or manager, you must be a good communicator, and thus, it is important to always improve our communication skills.

While there is no time-boxing for level progress, the company does operate on a [specific cycle](/handbook/people-group/360-feedback/) to make changes to a GitLab team-members level during the [360 Feedback](/handbook/people-group/360-feedback/).

## Interning with Infrastructure / Reliability Engineering

Cross reference for [Internship for Learning](/handbook/people-group/promotions-transfers/#internship-for-learning)

There is a reasonable amount of interest to intern with Infrastructure so we wanted to put a brief landing section in the handbook.  If you are interested in interning with Infrastructure, we would ask that you talk to your current manager and create an issue per the mentioned process.  This can help us track who is helping on what and what they would like to learn.  Mention the Reliability Engineering team members and managers to help us know you are interested.  One note, in some cases, you may need to submit access-requests to get access to certain systems to help with work.

Don't know what to learn? Check out the [SRE I](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/SRE%20Introduction.md) and the [SRE II](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/SRE%20II.md) Training Modules, these cover a range of topics related to how we do Reliability Engineering at GitLab. It is recommended that you come to your internship prepared, and by completing both modules, you'll ensure a richer internship experience as you'll make the best use of your time during your internship with an extra focus on more advanced topics.

Additionally, we have a broad set of technical skills listed in the [job description for SRE](/job-families/engineering/infrastructure/site-reliability-engineer/), if you are looking for ideas on what to learn, set up a coffee chat with a team member or one of the managers.
If you're interested in learning more about SREs in general, you may wish to read Alice Goldfuss's blog post, "[How to Get Into SRE](https://blog.alicegoldfuss.com/how-to-get-into-sre/)." This blog post is an overview of all sorts of SRE related things, some of which may not be in use here at GitLab. However, it can be a valuable tool to learn more about SREs in general, and it links to many resources that you can use to learn more about a variety of SRE related topics.

Specific ways to work in the internship:
1.  Pair up with an engineer on a particular issue or for our project work in OKRs.
2.  Shadow the On-Call in your similar timezone for 2 weeks
3.  Shadow the Delivery team to learn more about how we release to GitLab.com

## Staff+ IC Gearing in Infrastructure
GitLab Engineering employs a gearing method for determining the reporting relationship and number of Staff+ roles, generally referred to as [Staff+ IC Gearing Ratios](/handbook/engineering/career-development/#Staff+-IC-Gearing-Ratios). 

### Site Reliability Teams IC Gearing
The IC Gearing policy does not require any documentation for use of the default 1:1 ratio of Staff Engineer to team. However, to ensure clarity, the SRE teams within Infrastructure will use this default ratio, providing an opportunity for a Staff Engineer to be a part of each SRE team. As Infrastructure continues to grow and add more teams, additional opportunities for Staff level SRE leaders will be created.

### Delivery Team IC Gearing
**Exception Ratio**: 3 Staff Eng:Team

**Justification**: The Delivery team is tasked with making it increasingly safe and efficient for work product from all of GitLab to make its way to the production environment as well as into the monthly release, patch releases, and security releases. This is an area with continuing needs for additional automation as well as a wide scope of skills and experience.
The Delivery team requires multiple Staff Engineers with two frequently working in the [Solver archetype](/handbook/engineering/ic-leadership/#solver) on parallel issues (or staggered releases), while a third engages in an [Architect archetype](/handbook/engineering/ic-leadership/#architect) in moving automation and/or other CI/CD iterations forward.

**Future Growth or Anticipated Change**: It is expected that during FY22-FY23 two overall changes will influence the gearing ratio.
1. Increasing requirements for patch releases and complexity in overall release process will drive the creation of a second Delivery team
1. Maturity, efficiency, and automation of the CI/CD process will reach a level where there will remain much recurring process work, but a reduction in the large-scale architectural improvements by both Delivery teams.

As a result of the above it is expected that within the FY22-FY23 we will reach a point where each delivery team will reduce to a ratio of 2 Staff Eng:Team.

### Scalability Team IC Gearing
**Exception Ratio**: 2 Staff Eng:Team

**Justification**: The Scalability team is tasked with creating improvements for the highest priority scaling items, primarily in support of the GitLab SaaS service. The team regularly shifts from one area of product or platform to another and a wide range of backend and infrastructure experience and skills are required. Additionally, a keen mind for high level architecture and an understanding of product workflow are essential for success. The team generally has a scaling item they are currently working on as well as another item that is being evaluated for the next iteration. In this model the two Staff Engineers individually employ both [Architect and Solver archetypes](/handbook/engineering/ic-leadership/#the-four-archetypes-at-gitlab) towards their primary scaling item currently in focus.

**Future Growth or Anticipated Change**: It is expected that the team will continue to work in the current fashion where each Staff Engineer is providing technical leadership on parallel issues. In addition, it is expected that an additional 1-2 Scalability teams will be required in FY22-FY23 timeframe and likely that they will adopt the same model and Staff Eng ratio of 2 Staff Eng:Team.
