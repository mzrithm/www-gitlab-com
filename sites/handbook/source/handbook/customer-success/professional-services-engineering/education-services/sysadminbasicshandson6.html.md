---
layout: handbook-page-toc
title: "GitLab System Admin Basics Hands On Guide- Lab 6"
description: "This Hands On Guide Lab is designed to walk you through the lab exercises used in the GitLab System Admin Basics course."
---
# GitLab System Admin Basics Hands On Guide- Lab 6
{:.no_toc}


## LAB 6- ENHANCING SECURITY USING THE ADMIN CONSOLE

1. Log into your GitLab Instance with your admin username: root and password you created in Lab 1
2. On the main screen under the More tab at the top of the screen, click on **Admin Area** or click the Wrench icon.  
3. The first thing we want to do is adjust the Sign Up Restrictions.  
4. Scroll to the bottom of the left hand side bar and click **Settings** > **General**  
5. Under the Sign-up restrictions header, click the **Expand** button.  
6. Click the checkbox next to **Send Confirmation email on sign-up** to enable that feature.  
7. The next thing we want to do is add your company domain to the allow list to ensure sign ups are only allowed for that domain.  
8. In the **Allowed domains for sign ups window**, type your company’s domain name and press **Enter**.  
9. Scroll down to the end of the section and click the green **Save Changes** button. 

### SUGGESTIONS?

If you wish to make a change to our Hands on Guide for GitLab System Admin Basics- please submit your changes via Merge Request!

