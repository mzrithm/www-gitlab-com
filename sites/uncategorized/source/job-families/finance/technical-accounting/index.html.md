---
layout: job_family_page
title: "Technical Accounting Management"
description: "The Technical Accounting Role deeply understands GAAP accounting principles and how to apply those principles to GitLab’s financial statements."
---

The Technical Accounting Role deeply understands GAAP accounting principles and how to apply those principles to GitLab’s financial statements.

## Levels

### Manager, Technical Accounting

#### Job Grade

The Technical Accounting Manager is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities

* Assists in determining and documenting the Company’s accounting policies and position on relevant transactions.
* Review of non-recurring journal entries to determine appropriate classification.
* Must have good technical research skills.
* Must be able to work collaboratively with the operational accounting team and business functions.
* Provide periodic technical accounting trainings to the members of the Accounting organization.
* Coordinate with external audit (Ernst & Young) on technical accounting issues.
* This position will initially be an individual contributor but we expect this person to build a highly functioning, distributed team over time.
* Provide support to the accounting close process, including preparation of US GAAP financial statements.

#### Requirements

* Proven work experience in a Technical Accounting role.
* Fair knowledge of SEC filing requirements, experience highly preferred.
* Must have at least three years of work experience in a Big 4 CPA firm.
* Must have knowledge of US GAAP.
* Strong working knowledge of GAAP principles and financial statements.
* Proficient with google sheets.
* Revenue recognition, namely 606, experience highly preferred.
* Detail-oriented, self-directed and able to effectively complete tasks with minimal supervision
* You share our [values](/handbook/values/), and work in accordance with those values.
* Successful completion of a [background check](/handbook/legal/gitlab-code-of-business-conduct-and-ethics/#background-checks).
* [Leadership at GitLab](https://about.gitlab.com/company/team/structure/#management-group)
* Ability to use GitLab.

### Director, Technical Accounting

#### Job Grade

The Director, Technical Accounting Managers is a [grade 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities

All of the above responsibilities, plus
* Drives the positions GitLab has taken on revenue for ASC 606, commission capitalization and amortization, allocation of hosting costs, ProServe revenue, assists in the related party questionnaire, and other positions on US GAAP and SEC reporting documentation.
* Is the subject matter expert on SEC reporting, positions on new Accounting pronouncements, and other relevant technical accounting items.
* Manages a team of Technical Accounting Managers.
* Works with the Finance and Accounting teams on resolutions of issues related to revenue, expenses, balance sheet, and Audit Committee reports.
* Provides analysis and consultation of technical accounting to Financial leadership.

#### Requirements

All of the above requirements, plus
* Proven leadership of Technical Accounting team (hire, develop, coach, mentor, engage)
* Strong knowledge of SEC filing requirements, experience highly preferred.

## Career Ladder

The next step in the Technical Accounting Management job family is to move to the [Principal Accounting Officer](/job-families/finance/pao-jf/) job family. 

## Performance Indicators

* [Deals reviewed for revenue recognition](/handbook/finance/accounting/#deals-reviewed-for-revenue-recognition--100)
* [Issue opened for all key technical accounting matters](/handbook/finance/accounting/#issue-opened-for-all-key-technical-accounting-matters--2-working-days)
* [Key technical accounting matters to documented](/handbook/finance/accounting/#key-technical-accounting-matters-to-documented--5-working-days)
* [Technical accounting queries disposed of](/handbook/finance/accounting/#technical-accounting-queries-disposed-of--2-working-days)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).

- Selected candidates will be invited to schedule a 30 minute screening call with our Global Recruiters
- Next, candidates will be invited to schedule a 45 minute interview with our Senior Technical Accounting Manager
- Next, candidates will be invited to schedule a 45 minute interview with our Accounting and External Reporting Manager
- Candidates will then be invited to schedule a 45 minute interview with our CFO
- Finally, candidates may be asked to interview with the CEO
- Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring/interviewing/).
