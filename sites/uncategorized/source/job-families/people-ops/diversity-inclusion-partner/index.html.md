---
layout: job_family_page
title: "Diversity, Inclusion and Belonging"
---
The Diversity, Inclusion and Belonging (DIB) team builds an environment where all team members feel they belong: they are recognized, allowed to express their thoughts, have the ability to make contributions and are able to bring their best selves to work every day. As GitLab continues to grow, we look for people from a wide range of backgrounds to reflect the global nature of our customers and we want to have the tools and resources available to support that growth. It takes diversity of thought, culture, background, and perspective to build a truly global company. The DIB team members build and work with external partnerships as well as closely with internal departments such as Recruiting, Marketing, Communication, Talent Brand, etc to help shape the overall DIB strategy that creates, supports and fuels GitLab’s growth.

## Responsibilities
- Proactive support of all DIB projects throughout the organization
- Provide support to TMRGs to ensure effectiveness, compliance and overall alignment to company goals
- Support communication of DIB efforts across GitLab

## Requirements
- Ability to use GitLab
- Share our [values](/handbook/values/), and work in accordance with those values
- Strong business acumen and ability to connect DIB learning to business needs
- Skilled at taking a consultative and collaborative approach to problem solving
- Experience driving change management initiatives successfully and flexing with shifting priorities
- Strong project management and analytical skills
- Proven passion for diversity, inclusion and belonging
- Excellent EQ and an empathetic communication style that fosters connection, collaboration, and confidence
- Confidence to rely on own judgment and discretion to work independently, keep multiple plates spinning, and take smart risks
- Experience working in a global environment preferred
- Strong ability to plan proactively and react quickly when problems arise
- Strong communication skills

## Levels
### Diversity, Inclusion, and Belonging Specialist
The Diversity, Inclusion and Belonging Specialist reports to the [DIB Partner](#dib-partner).

#### Diversity, Inclusion, and Belonging Specialist Job Grade
The Diversity, Inclusion and Belonging Specialist is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Diversity, Inclusion, and Belonging Specialist Responsibilities
- Provide overall programmatic and strategy support to the DIB team 
- Deliver insights and coordinate DIB efforts across multiple functions 
- DIB monthly call action item follow up and communication of status 
- Develop content and tracks for internal / external events (virtual and in-person)
- Ensure DIB boards are moving along, labeled, organized, issues closed out as needed, monitor progress 
- Primary responder to DIB email, Slack channel to ensure response to team members at large here at GitLab
- Monitor training requests, update content, build new content
- Monitor and approve sponsorship request via the GitLab tool as in accordance with criteria for approval
- Provide support to DIB Partner for liaisons with external and internal PR teams including content creation
- Monitor and communicate the status of metric request

#### Diversity, Inclusion, and Belonging Specialist Requirements
- 1+ years of experience in human resources or related business experience
- 1+ years of experience leading diversity projects, diversity sourcing initiatives, and/or recruiting initiatives
- Foundational understanding of Diversity, Inclusion and Belonging 
- Ability to partner with leadership and team members

#### Diversity, Inclusion, and Belonging Specialist Performance Indicators
- [Engagement survey inclusion score](/company/culture/inclusion/#performance-indicators)

#### Diversity, Inclusion, and Belonging Specialist Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process.
* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
- 30-45 minute interview with the DIB Manager
- 30 minute interview with a member of the Learning and Development team 
- 30 minute interview with a People Business Partner
- 30 minute interview with someone on the People Success Operations team
- Final, the candidate may be asked to create a DIB related project/scenario approach

Additional details about our process can be found on our [hiring page](/handbook/hiring).

### Diversity, Inclusion, and Belonging Partner
The Diversity, Inclusion, and Belonging Partner reports to the [Manager, Diversity, Inclusion, and Belonging](#manager-diversity-inclusion-belonging).

#### Diversity, Inclusion, and Belonging Partner Job Grade 
The Diversity, Inclusion, and Belonging Partner is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Diversity, Inclusion, and Belonging Partner Responsibilities
- Extends the Diversity, Inclusion, and Belonging Specialist responsibilities
- Help build an environment where all team members feel they belong and can show up as their full selves by collaborating with all departments within GitLab as well as external
- Work closely as needed with the CEO and E-group and guided under the leadership of the DIB Manager
- Partner with the People Operations team, organizational leaders, and People Business Partners to continue building out our diversity, inclusion and belonging strategy
- Consult with organizational leaders to provide mentorship and coaching on how to achieve results
- Works with the Recruiting team to create and implement a diversity recruitment strategy
- Develop and improve relationships with current and new diversity organizations and partnerships
- Increase organizational capabilities in cross-cultural intelligence, mitigating unconscious bias, and inclusive leadership
- Provide communication and visibility of DIB efforts across GitLab
- Design leadership development programs focused on underrepresented groups
- Review and analyze data to monitor progress
- Conduct research and benchmarking on DIB standard methodologies and emerging trends and apply that to relevant programs aligned to business needs
- Collaborate strategically to engage team members, amplify successes, and ensure inclusive internal and external messaging
- Build out a library of diversity, inclusion and belonging materials

#### Diversity, Inclusion, and Belonging Partner Requirements
- Extends the Diversity, Inclusion, and Belonging Specialist requirements
- 5+ years of experience in human resources or related business experience
- 3+ years of experience leading diversity projects, diversity sourcing initiatives, and/or recruiting initiatives
- 3+ years leadership experience, preferably in the DIB space
- Deep understanding of diversity, inclusion and belonging principles and practices
- Ability to influence leadership and enroll team member support and engagement
- Experienced facilitation and communication skills

#### Diversity, Inclusion, and Belonging Partner Performance Indicators
- [Women globally as a whole at GitLab](/company/culture/inclusion/#performance-indicators)
- [Women in leadership](/company/culture/inclusion/#performance-indicators)
- [Women voluntary attrition](/company/culture/inclusion/#performance-indicators)
- [Pay equality](/company/culture/inclusion/#performance-indicators)
- [Engagement survey inclusion score](/company/culture/inclusion/#performance-indicators)

#### Diversity, Inclusion, and Belonging Partner Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process.
* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
- Next, candidates will be invited to schedule a 45 minute panel interview with one of our People Business Partners, member of our L&D team and People Operations leadership
- Next, candidates will be invited to schedule a 45 minute interview with our Senior Director, People Success 
- After that, candidates will be invited to schedule a 30 minute interview with our Chief People Officer
- After that, the candidate may be invited to interview with our CFO or another member of our executive team
- Finally, our CEO may choose to conduct a final interview

Additional details about our process can be found on our [hiring page](/handbook/hiring).

### Manager, Diversity Inclusion, and Belonging
The Manager, Diversity Inclusion, and Belonging reports to the [Senior Director, People Success](/job-families/people-ops/people-leadership/#senior-director-people-success).

#### Manager, Diversity Inclusion, and Belonging Job Grade 
The Diversity, Inclusion, and Belonging Partner is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Manager, Diversity Inclusion, and Belonging Responsibilities
- Extends the Diversity, Inclusion, and Belonging Partner responsibilities
- Partner with People Group Leaders to publish an easily understood DIB plan  
- Provide coaching and development to DIB team members to aid their career advancement - monitoring  progress of issues and mentor / coach DIB team members on how to get issues to fruition.  Analysis and monitor capacity of DIB team and drive efficiencies and automations to free up capacity as identified
- Translate GitLab's DIB aspiration and plan into programmatic outcomes and an implementation roadmap.  Defining roles and responsibilities of the DIB team in alignment with GitLab's overall goals. Efficiently manage programs against approved DIB budgets. Conduct quarterly reviews of DIB sponsorships in an effort to understand and maintain budget allotment and spend.  Advise necessary adjustments to the sponsorship process. Prepare DIB materials as needed for inclusion in presentations
- Serves as an DIB subject matter expert by giving perspective, insights, and knowledge in a consultative manner; partnering with all leaders and team members to implement DIB initiatives.  Work closely with the E Group and all leaders across the company to advise, influence, build practices and drive implementation of programs to affect the Company’s DIB strategy
- Partner with Communications, Legal, Recruiting and other internal teams to produce GitLabs Annual DIB, survey, DIB messaging for GitLab, CEO, ICs, TMRGs, leaders, etc.  Act as DIB contact point when working with CEO and PR teams both internal and external on company DIB responses.  Provide additional guidance in support to the DIB Partner and or Specialist of GitLabs TMRGs in how they can align to work being done on the leadership and executive level, growth opportunities, connection opportunities, etc
- Partner with and advise Recruiting, Talent Brand teams and partner resources to ensure the attraction and engagement of a talented and diverse workforce.  Determine DIB related surveys to participate, in an effort to increase GitLabs DIB brand, opportunities for benchmarking against other companies and increase talent brand/attraction
- In partnership with PBP and leaders, develop strategies for their respective departments based on metrics  
- Represent GitLab in various DIB related media/press discussion, interviews, etc
- Maintain relationships with diversity related orgs and community, with the intention of working together and improving GitLab brand
- Serve as a subject matter expert and partner to the business by continuously analyzing and presenting talent demographics, monitoring data trends, and providing insights into the metrics collected

#### Manager, Diversity Inclusion, and Belonging Requirements
- Extends the Diversity, Inclusion, and Belonging Partner requirements
- 7+ years of experience in human resources or related business experience
- 5+ years of experience leading diversity projects, diversity sourcing initiatives, and/or recruiting initiatives
- 3+ years leadership experience, preferably in the DIB space
- 3+ years of strong people management experience 
- [Bias towards action](/handbook/values/#bias-for-action) 
- Proven ability to achieve results 
- Excellent program / project management and analytical skills
- Proven ability to drive a globally inclusive DIB agenda 

#### Manager, Diversity Inclusion, and Belonging Performance Indicators
- Extends the Diversity, Inclusion, and Belonging Partner Performance Indicators

#### Manager, Diversity Inclusion, and Belonging Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process.
* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
- Next, candidates will be invited to schedule a 45 minute panel interview with one of our People Business Partners, member of our L&D team and People Operations leadership
- Next, candidates will be invited to schedule a 45 minute interview with our Senior Director, People Success 
- After that, candidates will be invited to schedule a 30 minute interview with our Chief People Officer
- After that, the candidate may be invited to interview with our CFO or another member of our executive team
- Finally, our CEO may choose to conduct a final interview

Additional details about our process can be found on our [hiring page](/handbook/hiring).

## Career Ladder
The next step in the Diversity, Inclusion, and Belonging job family is to move to a senior manager role which is not yet defined at GitLab. 
